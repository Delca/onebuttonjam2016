﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BattleAI : InstructionProvider
{
    BattleSystem battle_;

    void Awake()
    {
        battle_ = FindObjectOfType<BattleSystem>();
    }

    public override void RequestInstruction(Fighter caster, Action callback)
    {
        var usableSkills = new List<Skill>(from s in caster.entity.skills where caster.entity.CanUseSkill(s) select s);

        // Skip turn if no skill can be used
        if (usableSkills.Count == 0)
        {
            callback.Invoke();
            return;
        }

        var randomSkillIndex = UnityEngine.Random.Range(0, usableSkills.Count);
        var skill = usableSkills[randomSkillIndex];

        if (skill.element == Element.Heal)
        {
            var allies = new List<Fighter>(battle_.GetFighterAllies(caster));
            allies.Sort((Fighter lhs, Fighter rhs) => { return Mathf.RoundToInt((rhs.entity.maxHp - rhs.entity.hp) - (lhs.entity.maxHp - lhs.entity.hp)); });
            skill.Cast(caster, allies[0], callback);
        }
        else
        {
            var enemies = battle_.GetFigtherEnemies(caster);
            skill.Cast(caster, enemies[UnityEngine.Random.Range(0, enemies.Count)], callback);
        }
    }
}
