﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class BattleSystem : MonoBehaviour
{
    public Transform playerFighterPrefab;
    public Transform computerFighterPrefab;

    public Transform fighterStatsDisplayPrefab;

    public float horizontalSpace;
    public float verticalSpace;
    public float statDisplayOffset = 1.5f;

    public PlayerInstructionProvider playerInputHandler;

    int fighterIndex_;
    List<Fighter> fighters_ = new List<Fighter>();
    List<Fighter> playerParty_ = new List<Fighter>();
    List<Fighter> computerParty_ = new List<Fighter>();

    public static IEnumerator OpenBattleScene(List<Entity> playerEntities, List<Entity> computerEntities, MusicManager.MusicTrack battleTheme = MusicManager.MusicTrack.BATTLE)
    {
        var asyncLoading = SceneManager.LoadSceneAsync("Battle", LoadSceneMode.Additive);
        while (!asyncLoading.isDone)
            yield return null;

        var overworldParentObject = OverworldManager.instance.gameObject.transform.parent.gameObject;
        overworldParentObject.SetActive(false);

        var battleSystem = FindObjectOfType<BattleSystem>();
        battleSystem.StartBattle(playerEntities, computerEntities, battleTheme);
    }

    public void StartBattle(List<Entity> playerEntities, List<Entity> computerEntities, MusicManager.MusicTrack battleTheme = MusicManager.MusicTrack.BATTLE)
    {
        var playerTurnUI = FindObjectOfType<PlayerTurnUI>();
        var canvas = playerTurnUI.GetComponentInParent<Canvas>();
        Vector3 canvasCenterPosition = canvas.GetComponent<RectTransform>().sizeDelta * .5f;

        for (int i = 0; i < playerEntities.Count; ++i)
        {
            var fighterTransform = Instantiate(playerFighterPrefab);
            fighterTransform.SetParent(transform, false);

            var position = new Vector3(horizontalSpace * .5f, (.5f * (1 - playerEntities.Count) + i) * verticalSpace);
            fighterTransform.localPosition = position;

            var fighter = fighterTransform.GetComponent<Fighter>();
            playerParty_.Add(fighter);

            var statsDisplayTransform = Instantiate(fighterStatsDisplayPrefab);
            statsDisplayTransform.SetParent(canvas.transform, false);
            statsDisplayTransform.localPosition = Camera.main.WorldToScreenPoint(fighter.transform.position + Vector3.right * statDisplayOffset) - canvasCenterPosition;

            var statsDisplay = statsDisplayTransform.GetComponent<FighterStatsDisplay>();
            statsDisplay.fighter = fighter;

            fighter.entity = playerEntities[i];
            fighter.instructionProvider = playerInputHandler;
            fighterTransform.gameObject.name = fighter.entity.name;
        }

        for (int i = 0; i < computerEntities.Count; ++i)
        {
            var fighterTransform = Instantiate(computerFighterPrefab);
            fighterTransform.SetParent(transform, false);

            var position = new Vector3(horizontalSpace * -.5f, (.5f * (1 - computerEntities.Count) + i) * verticalSpace);
            fighterTransform.localPosition = position;

            var fighter = fighterTransform.GetComponent<Fighter>();
            computerParty_.Add(fighter);

            var statsDisplayTransform = Instantiate(fighterStatsDisplayPrefab);
            statsDisplayTransform.SetParent(canvas.transform, false);
            statsDisplayTransform.localPosition = Camera.main.WorldToScreenPoint(fighter.transform.position + Vector3.left * statDisplayOffset) - canvasCenterPosition;

            var statsDisplay = statsDisplayTransform.GetComponent<FighterStatsDisplay>();
            statsDisplay.fighter = fighter;

            fighter.entity = computerEntities[i];
            fighterTransform.gameObject.name = fighter.entity.name;
        }

        fighters_.AddRange(playerParty_);
        fighters_.AddRange(computerParty_);

        // Sort fighters from the fastest to the slowest
        fighters_.Sort((Fighter lhs, Fighter rhs) => { return rhs.entity.speed - lhs.entity.speed; });
        fighterIndex_ = 0;

        // Remove dead Fighters
        for (int i = fighters_.Count - 1; i >= 0; --i)
        {
            var fighter = fighters_[i];
            if (fighter.entity.hp < float.Epsilon)
                RemoveFromBattle(fighter);
        }

        if (MusicManager.instance != null)
        {
            MusicManager.instance.PlayTrack(battleTheme);
        }

        StartCoroutine(BeginTurn());
    }

    public void RemoveFromBattle(Fighter fighter)
    {
        var removedIndex = fighters_.IndexOf(fighter);
        fighters_.RemoveAt(removedIndex);

        if (fighterIndex_ > removedIndex)
            --fighterIndex_;

        if (playerParty_.Contains(fighter))
            playerParty_.Remove(fighter);
        else
            computerParty_.Remove(fighter);

        fighter.gameObject.SetActive(false);
        Debug.Log(fighter.name + " was removed from battle");
    }

    public List<Fighter> GetFighterAllies(Fighter caster)
    {
        return (playerParty_.Contains(caster)) ? playerParty_ : computerParty_;
    }

    public List<Fighter> GetFigtherEnemies(Fighter caster)
    {
        return (playerParty_.Contains(caster)) ? computerParty_ : playerParty_;
    }

    IEnumerator BeginTurn()
    {
        yield return new WaitForSeconds(.5f);
        fighters_[fighterIndex_].BeginTurn(EndTurn);
    }

    void EndTurn()
    {
        fighterIndex_ = (fighterIndex_ + 1) % fighters_.Count;

        if ((playerParty_.Count == 0) || (computerParty_.Count == 0))
            CloseBattleScene();
        else
            StartCoroutine(BeginTurn());
    }

    public void CloseBattleScene()
    {
        if (SceneManager.sceneCount == 1)
        {
            Debug.Log("Overworld is not loaded.");
            return;
        }

        if (playerParty_.Count == 0)
        {
            SceneManager.LoadScene("GameOver");
            return;
        }
        SceneManager.UnloadScene("Battle");

        SoundEffectManager.instance.PlaySound(SoundEffectManager.SoundEffect.VICTORY);
        if (MusicManager.instance != null)
        {
            MusicManager.instance.PlayTrack(MusicManager.MusicTrack.DUNGEON);
        }

        var overworldParentObject = OverworldManager.instance.gameObject.transform.parent.gameObject;
        overworldParentObject.SetActive(true);
        GameObject.FindGameObjectWithTag("Player").GetComponent<MovingObject>().isMoving = false;
    }
}
