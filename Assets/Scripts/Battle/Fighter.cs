﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
//[RequireComponent(typeof(Animator))]
public class Fighter : MonoBehaviour
{
    public Entity entity;
    public InstructionProvider instructionProvider;

    public float forwardStepDistance = .1f;

    [Range(.1f, 1f)]
    public float bumpTravelTime;
    [Range(.1f, 1f)]
    public float returnTravelTime;

    BattleSystem battle_;
    SpriteRenderer renderer_;

    void Awake()
    {
        battle_ = FindObjectOfType<BattleSystem>();
        renderer_ = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        Debug.Assert(entity != null);
        renderer_.sprite = entity.sprite;
    }

    public void ApplyDamage(Element element, int value)
    {
        entity.ApplyDamage(element, value);
        if (entity.hp == 0)
        {
            battle_.RemoveFromBattle(this);
            if (SoundEffectManager.instance != null)
            {
                SoundEffectManager.instance.PlaySound(entity.deathSound);
            }
        }
    }

    public void BeginTurn(Action callback)
    {
        transform.Translate(((transform.localPosition.x < 0) ? Vector3.right : Vector3.left) * forwardStepDistance);

        instructionProvider.RequestInstruction(this, new Action(() => { EndTurn(callback); }));
    }

    public IEnumerator BumpTarget(Fighter target, Action onContactcallback, Action onReturnCallback)
    {
        var timer = bumpTravelTime;
        var originalPosition = transform.localPosition;

        while (timer > 0)
        {
            var progress = 1 - timer / bumpTravelTime;
            transform.localPosition = Vector3.Lerp(originalPosition, target.transform.localPosition, progress);

            timer -= Time.deltaTime;
            yield return null;

        }
        onContactcallback.Invoke();
        yield return null;

        timer = returnTravelTime;
        while (timer > 0)
        {
            var progress = timer / returnTravelTime;
            transform.localPosition = Vector3.Lerp(originalPosition, target.transform.localPosition, progress);

            timer -= Time.deltaTime;
            yield return null;
        }

        transform.localPosition = originalPosition;

        onReturnCallback.Invoke();
    }

    void EndTurn(Action callback)
    {
        transform.Translate(((transform.localPosition.x < 0) ? Vector3.left : Vector3.right) * forwardStepDistance);
        callback.Invoke();
    }
}
