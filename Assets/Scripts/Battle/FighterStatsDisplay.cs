﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class FighterStatsDisplay : MonoBehaviour
{
    public Fighter fighter;
    public Gauge healthBar;
    public Gauge manaBar;
    public Text healthLabel;
    public Text manaLabel;

    public void Update()
    {
        if (fighter == null)
            return;
        if (healthBar != null)
            healthBar.Fill(fighter.entity.hp / fighter.entity.maxHp);

        if (manaBar != null)
            manaBar.Fill(fighter.entity.mp / fighter.entity.maxMp);

        if (healthLabel != null)
            healthLabel.text = fighter.entity.hp.ToString();

        if (manaLabel != null)
            manaLabel.text = fighter.entity.mp.ToString();
    }
}
