﻿using System;
using System.Collections;
using UnityEngine;

public abstract class InstructionProvider : MonoBehaviour
{
    public abstract void RequestInstruction(Fighter caster, Action callback);
}
