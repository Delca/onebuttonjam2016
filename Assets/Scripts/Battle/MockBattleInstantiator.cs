﻿using UnityEngine;
using System.Collections.Generic;

public class MockBattleInstantiator : MonoBehaviour
{
    public List<EntityData> playerEntityData;
    public List<EntityData> computerEntityData;

    void Start()
    {
        if (UnityEngine.SceneManagement.SceneManager.sceneCount > 1)
            return;

        var playerEntities = new List<Entity>();
        foreach (var entityData in playerEntityData)
            playerEntities.Add(new Entity(entityData));

        var computerEntities = new List<Entity>();
        foreach (var entityData in computerEntityData)
            computerEntities.Add(new Entity(entityData));


        var battleSystem = FindObjectOfType<BattleSystem>();
        battleSystem.StartBattle(playerEntities, computerEntities);
    }
}
