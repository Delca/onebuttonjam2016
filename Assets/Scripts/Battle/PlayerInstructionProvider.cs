﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInstructionProvider : InstructionProvider
{
    [Range(.1f, 1f)]
    public float beatDuration = .5f;

    public PlayerTurnUI playerUI;

    BattleSystem battle_;
    Fighter caster_;
    Action onTurnFinishedCallback_;

    void Start()
    {
        battle_ = FindObjectOfType<BattleSystem>();
    }

    IEnumerator SelectSkill()
    {
        while (!Input.GetButtonDown("Button"))
            yield return null;

        List<bool> buttonStates = new List<bool>();

        while (buttonStates.Count < 4)
        {
            float timer = beatDuration;
            while (timer > 0)
            {
                timer -= Time.deltaTime;
                var progress = Mathf.Min(1, 1 - (timer / beatDuration));
                playerUI.SetCastProgress((buttonStates.Count + progress) * .25f);
                yield return null;
            }
            bool buttonPress = Input.GetButton("Button");
            buttonStates.Add(buttonPress);
            playerUI.SetPattern(buttonStates);
            if (buttonPress)
            {
                if (SoundEffectManager.instance != null)
                {
                    SoundEffectManager.instance.PlaySound(SoundEffectManager.SoundEffect.BEEPLOW);
                }
            }
            else
            {
                if (SoundEffectManager.instance != null)
                {
                    SoundEffectManager.instance.PlaySound(SoundEffectManager.SoundEffect.BEEPHIGH);
                }
            }
        }

        var pattern = new SkillPattern(buttonStates);

        var skill = caster_.entity.GetSkill(pattern);
        if ((skill == null) || (!caster_.entity.CanUseSkill(skill)))
        {
            TerminateRequest();
            SoundEffectManager.instance.PlaySound(SoundEffectManager.SoundEffect.ACTIONMISS);
        }
        else
            StartCoroutine(SelectTarget(skill));
    }

    IEnumerator SelectTarget(Skill skill)
    {
        int targetIndex = 0;
        var targets = (skill.element == Element.Heal) ? battle_.GetFighterAllies(caster_) : battle_.GetFigtherEnemies(caster_);

        playerUI.ShowCursor(targets[0]);

        float timer = beatDuration;
        while (!Input.GetButtonDown("Button"))
        {
            yield return null;
            if (timer > 0)
                timer -= Time.deltaTime;
            else
            {
                timer = beatDuration;
                targetIndex = (targetIndex + 1) % targets.Count;
                playerUI.UpdateCursorPosition(targets[targetIndex]);
            }
        }

        playerUI.HideCursor();

        skill.Cast(caster_, targets[targetIndex], TerminateRequest);
    }

    void TerminateRequest()
    {
        caster_ = null;
        onTurnFinishedCallback_.Invoke();
        onTurnFinishedCallback_ = null;
    }

    public override void RequestInstruction(Fighter caster, Action onTurnFinishedCallback)
    {
        caster_ = caster;
        onTurnFinishedCallback_ = onTurnFinishedCallback;

        playerUI.SetFighter(caster);

        StartCoroutine(SelectSkill());
    }
}
