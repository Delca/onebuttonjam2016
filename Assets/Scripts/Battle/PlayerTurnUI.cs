﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(CanvasGroup))]
public class PlayerTurnUI : MonoBehaviour
{
    public Text nameLabel;
    public Gauge castGauge;
    public SkillPatternDisplay patternDisplay;
    public List<SkillDisplay> skillDisplayList;

    public Image targetCursor;
    public float horizontalCursorOffset = .2f;

    CanvasGroup canvasGroup_;
    Vector3 canvasCenterPosition_;

    void Awake()
    {
        canvasGroup_ = GetComponent<CanvasGroup>();
        SetFighter(null);
    }

    void Start()
    {
        canvasCenterPosition_ = FindObjectOfType<Canvas>().GetComponent<RectTransform>().sizeDelta * .5f;
    }

    public void SetCastProgress(float progress)
    {
        castGauge.Fill(progress);
    }

    public void SetPattern(List<bool> pattern)
    {
        patternDisplay.ClearPattern();
        patternDisplay.SetPattern(pattern, 4);
    }

    public void ShowCursor(Fighter initialTarget)
    {

        targetCursor.color = Color.white;

        bool invertCursorX = initialTarget.transform.localPosition.x > 0;

        var cursorScale = targetCursor.transform.localScale;
        cursorScale.x = invertCursorX ? -1 : 1;
        targetCursor.transform.localScale = cursorScale;

        UpdateCursorPosition(initialTarget);
    }

    public void UpdateCursorPosition(Fighter target)
    {
        bool invertCursorX = target.transform.localPosition.x > 0;

        var cursorOffset = (invertCursorX ? Vector3.left : Vector3.right) * horizontalCursorOffset;
        var cursorPosition = Camera.main.WorldToScreenPoint(target.transform.position + cursorOffset);
        targetCursor.transform.localPosition = cursorPosition - canvasCenterPosition_;
    }

    public void HideCursor()
    {
        targetCursor.color = Color.clear;
    }

    public void SetFighter(Fighter fighter)
    {
        if (fighter == null)
        {
            canvasGroup_.alpha = 0;
            return;
        }

        canvasGroup_.alpha = 1;

        nameLabel.text = fighter.name;

        for (int i = 0; i < 4; ++i)
        {
            var skill = fighter.entity.skills[i];
            skillDisplayList[i].SetSkill(skill, fighter.entity.CanUseSkill(skill));
        }

        SetCastProgress(0);
        SetPattern(new List<bool>());
    }
}
