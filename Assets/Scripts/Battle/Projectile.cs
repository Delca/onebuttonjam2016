﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class Projectile : MonoBehaviour
{
    public AnimationCurve speedCurve;
    public float speed;
    public float afterImpactDelay;

    Vector3 origin_;
    Vector3 target_;

    Action onImpactCallback_;
    Action onTurnEndCallback_;

    SpriteRenderer renderer_;

    void Awake()
    {
        renderer_ = GetComponent<SpriteRenderer>();
    }

    public void Fire(Fighter caster, Fighter target, Action onImpactCallback, Action onTurnEndCallback)
    {
        origin_ = caster.transform.position;
        target_ = target.transform.position;

        onImpactCallback_ = onImpactCallback;
        onTurnEndCallback_ = onTurnEndCallback;

        transform.position = origin_;

        StartCoroutine(FireCoroutine());
    }

    IEnumerator FireCoroutine()
    {
        var travelVector = target_ - origin_;
        var travelDistance = travelVector.magnitude;
        travelVector.Normalize();

        var travelTime = travelDistance / speed;
        var timer = travelTime;
        while (timer > 0)
        {
            var progress = 1 - (timer / travelTime);
            transform.position = origin_ + travelVector * speedCurve.Evaluate(progress) * travelDistance;

            timer -= Time.deltaTime;
            yield return null;
        }
        onImpactCallback_.Invoke();

        renderer_.enabled = false;
        yield return new WaitForSeconds(afterImpactDelay);

        onTurnEndCallback_.Invoke();
        Destroy(gameObject);
    }
}
