﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(CanvasGroup))]
public class SkillDisplay : MonoBehaviour
{
    public Image elementIcon;
    public List<Sprite> elementIconSprites;

    public Text nameLabel;
    public Text mpCostLabel;

    public SkillPatternDisplay patternDisplay;

    [Range(0, 1)]
    public float alphaWhenInactive = .5f;

    CanvasGroup canvasGroup_;

    void Awake()
    {
        canvasGroup_ = GetComponent<CanvasGroup>();
    }

    public void SetSkill(Skill skill, bool isActive)
    {
        elementIcon.sprite = elementIconSprites[(int)skill.element];

        nameLabel.text = skill.name;
        mpCostLabel.text = skill.mpCost.ToString();

        if (patternDisplay.transform.childCount > 0)
            patternDisplay.ClearPattern();
        patternDisplay.SetPattern(skill.pattern);

        canvasGroup_.alpha = isActive ? 1 : alphaWhenInactive;
    }
}
