﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillPatternDisplay : MonoBehaviour
{
    public Transform bulletPrefab;
    public Transform arrowPrefab;

    public Sprite upArrow;
    public Sprite downArrow;

    public float horizontalSpace;

    public void SetPattern(SkillPattern pattern)
    {
        var arrowSize = arrowPrefab.GetComponent<RectTransform>().sizeDelta;

        for (int i = 0; i < pattern.buttonStates.Length; ++i)
        {
            var arrowTransform = Instantiate(arrowPrefab);
            arrowTransform.SetParent(transform, false);

            var localPosition = new Vector2(i * (arrowSize.x + horizontalSpace), 0);
            arrowTransform.localPosition = localPosition;

            var arrowImage = arrowTransform.GetComponent<Image>();
            arrowImage.sprite = pattern.buttonStates[i] ? downArrow : upArrow;
        }
    }

    public void SetPattern(List<bool> pattern, int maxLength)
    {
        var arrowSize = arrowPrefab.GetComponent<RectTransform>().sizeDelta;

        var arrowCount = Mathf.Min(maxLength, pattern.Count);
        int i = 0;
        while (i < arrowCount)
        {
            var arrowTransform = Instantiate(arrowPrefab);
            arrowTransform.SetParent(transform, false);

            var localPosition = new Vector2(i * (arrowSize.x + horizontalSpace), 0);
            arrowTransform.localPosition = localPosition;

            var arrowImage = arrowTransform.GetComponent<Image>();
            arrowImage.sprite = pattern[i] ? downArrow : upArrow;
            ++i;
        }

        while (i < maxLength)
        {
            var bulletTransform = Instantiate(bulletPrefab);
            bulletTransform.SetParent(transform, false);

            var localPosition = new Vector2(i * (arrowSize.x + horizontalSpace), 0);
            bulletTransform.localPosition = localPosition;
            ++i;
        }
    }

    public void ClearPattern()
    {
        transform.DestroyChildren();
    }
}
