﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class ConstantViewportSetter : MonoBehaviour
{
    public int width, height;

    private Camera m_camera;

    // Use this for initialization
    void Start()
    {
        m_camera = GetComponent<Camera>();
        m_camera.pixelRect = new Rect(0, 0, width, height);
        m_camera.orthographicSize = height / 64f;
    }

#if UNITY_EDITOR
    // Update is called once per frame
    void Update()
    {
        m_camera.pixelRect = new Rect(0, 0, width, height);
    }

    void OnPreRender()
    {
        m_camera.pixelRect = new Rect(0, 0, width, height);
    }
#endif // UNITY_EDITOR
}
