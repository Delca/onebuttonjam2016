﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyBalancer : MonoBehaviour {

    public static Entity[] players = null;

    public EntityData strongEnemy;
    public EntityData magicEnemy;
    public EntityData quickEnemy;

    public EntityData bossEnemy;
	
    int Average(Entity[] entities, Func<Entity, int> getValue)
    {
        if (entities.Length <= 0)
        {
            return 0;
        }

        int ans = 0;

        foreach (Entity e in entities)
        {
            ans += getValue(e);
        }

        return ans / entities.Length;
    }

    public void RebalanceEnemies()
    {
        GameObject playerGO = GameObject.FindGameObjectWithTag("Player");
        
        if (playerGO != null)
        {
            players = playerGO.GetComponent<Player>().entities.ToArray();
        }

        if (players == null || players.Length <= 0)
        {
            Debug.Log("No players could be found to balance against");
            return;
        }

        int averageHp = Average(players, (Entity e) => { return e.maxHp; }),
            averageMp = Average(players, (Entity e) => { return e.maxMp; }),
            averageAttack = Average(players, (Entity e) => { return e.attack; }),
            averageDefense = Average(players, (Entity e) => { return e.defense; }),
            averageMagic = Average(players, (Entity e) => { return e.magic; }),
            averageSpeed = Average(players, (Entity e) => { return e.speed; });

        strongEnemy.balanceBonus = new int[] { Math.Max(0, averageHp - strongEnemy._maxHp),
                                               Math.Max(0, averageMp - strongEnemy._maxMp),
                                               Math.Max(0, 2*averageDefense - strongEnemy._attack),
                                               0,
                                               0,
                                               Math.Max(0, averageSpeed/2 - strongEnemy._speed)
                                             };
        magicEnemy.balanceBonus = new int[] { Math.Max(0, averageHp/2 - magicEnemy._maxHp),
                                               Math.Max(0, 2*averageMp - magicEnemy._maxMp),
                                               0,
                                               magicEnemy.balanceBonus[3] + 2,
                                               Math.Max(0, 2*averageMagic - magicEnemy.magic),
                                               Math.Max(0, averageSpeed/4 - magicEnemy._speed)
                                             };

        quickEnemy.balanceBonus = new int[] { Math.Max(0, averageHp/2 - magicEnemy._maxHp),
                                               Math.Max(0, averageMp - magicEnemy._maxMp),
                                               Math.Max(0, averageDefense - strongEnemy._attack),
                                               0,
                                               Math.Max(0, averageMagic - magicEnemy.magic),
                                               Math.Max(0, 2*averageSpeed - magicEnemy._speed)
                                             };

        bossEnemy.balanceBonus = new int[] { Math.Max(0, 2*averageHp - bossEnemy._maxHp),
                                               Math.Max(0, averageMp - bossEnemy._maxMp),
                                               Math.Max(0, 2*averageDefense - bossEnemy._attack),
                                               Math.Max(0, 2*averageAttack - bossEnemy._defense),
                                               Math.Max(0, 2*averageMagic - bossEnemy._magic),
                                               Math.Max(0, 3*averageSpeed/2 - bossEnemy._speed)
                                             };

        Debug.Log("Rebalanced strong enemy with " + strongEnemy.balanceBonus.ToString());

    }
}
