﻿using UnityEngine;
using System.Collections.Generic;

public class Entity
{
    public readonly string name;
    public int maxHp = 10;
    public int maxMp = 10;

    public int attack = 5;
    public int defense = 5;
    public int magic = 5;
    public int speed = 5;

    public EntityData.EntityClass entityClass;
    public Sprite sprite = null;
    public SoundEffectManager.SoundEffect deathSound = SoundEffectManager.SoundEffect.NONE;
    public readonly List<Skill> skills;

    public float hp { get; private set; }
    public float mp { get; private set; }

    public EntityData entityData;

    public Entity(EntityData data)
    {
        entityData = data;

        name = data.name;

        maxHp = data._maxHp;
        maxMp = data._maxMp;

        attack = data._attack;
        defense = data._defense;
        magic = data._magic;
        speed = data._speed;

        hp = maxHp;
        mp = maxMp;

        entityClass = data.entityClass;
        sprite = data.sprite;
        deathSound = data.deathSound;
        skills = data.skills;
    }

    public Skill GetSkill(SkillPattern pattern)
    {
        foreach (var skill in skills)
        {
            if (skill.pattern == pattern)
                return skill;
        }
        return null;
    }

    public bool CanUseSkill(Skill skill)
    {
        return (skill.mpCost <= mp);
    }

    public void ApplyDamage(Element element, int value)
    {
        Debug.Assert(value >= 0);
        if (element == Element.Heal)
            hp = Mathf.Min(maxHp, hp + value);
        else
            hp = Mathf.Max(0, hp - value);
    }

    public void ConsummeMp(int value)
    {
        Debug.Assert(value >= 0);
        mp = Mathf.Max(0, mp - value);
    }

    public void RestoreMp(int value)
    {
        Debug.Assert(value >= 0);
        mp = Mathf.Min(maxMp, mp + value);
    }
}
