﻿using UnityEngine;
using System.Collections.Generic;

public class EntityData : ScriptableObject
{
    public enum EntityClass { NONE, ROGUE, MAGE, KNIGHT, ORK, SHAMAN, GOBLIN, MIMIC };

    [System.NonSerialized]
    [HideInInspector]
    public int[] balanceBonus = new int[] { 0, 0, 0, 0, 0, 0 };

    public int maxHp = 10;
    public int maxMp = 10;
    public int attack = 5;
    public int defense = 5;
    public int magic = 5;
    public int speed = 5;

    public int _maxHp { get { return maxHp + balanceBonus[0]; } set { maxHp = value; } }
    public int _maxMp { get { return maxMp + balanceBonus[1]; } set { maxMp = value; } }

    public int _attack { get { return attack + balanceBonus[2]; } set { attack = value; } }
    public int _defense { get { return defense + balanceBonus[3]; } set { defense = value; } }
    public int _magic { get { return magic + balanceBonus[4]; } set { magic = value; } }
    public int _speed { get { return speed + balanceBonus[5]; } set { speed = value; } }

    public Sprite sprite;
    public SoundEffectManager.SoundEffect deathSound;
    public List<Skill> skills;
    public EntityClass entityClass;

    public string[] dialogs;
    
#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Entity")]
    public static void Create()
    {
        var asset = CreateInstance<EntityData>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "New Entity", "Entities");
    }
#endif //UNITY_EDITOR
}
