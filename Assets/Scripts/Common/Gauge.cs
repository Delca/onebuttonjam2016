﻿using UnityEngine;
using UnityEngine.UI;

public class Gauge : MonoBehaviour
{
    public Image fillImage;

    float maxWidth_;

    void Awake()
    {
        maxWidth_ = fillImage.rectTransform.sizeDelta.x;
    }

    public void Fill(float fillAmount)
    {
        Debug.Assert(fillAmount >= 0 && fillAmount <= 1);

        if (fillImage.type == Image.Type.Filled)
        {
            fillImage.fillAmount = fillAmount;
        }
        else
        {
            float height = fillImage.rectTransform.sizeDelta.y;
            fillImage.rectTransform.sizeDelta = new Vector2(maxWidth_ * fillAmount, height);
        }
    }
}
