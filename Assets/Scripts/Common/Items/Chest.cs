﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using System;

public class Chest : UnlockableItem
{
    public ItemData[] content;
    public int randomChances = 0; // How many items will be randomly spawned. 0 spawns it all.

    public override bool OnItemPickup(Item item)
    {
        SoundEffectManager.instance.PlaySound(SoundEffectManager.SoundEffect.CHESTOPEN);
        
        if (randomChances > 0)
        {
            ItemData[] toSpawn = new ItemData[randomChances];
            
            for (int i = 0; i < randomChances; ++i) {
                toSpawn[i] = content[Random.Range(0, content.Length)];
            }

            OverworldManager.instance.ScatterItemsAround(item.xMap, item.yMap, toSpawn);
        }
        else
        {
            OverworldManager.instance.ScatterItemsAround(item.xMap, item.yMap, content);
        }

        return true;
    }

    public override bool OnItemUse(Item item)
    {
        return false;
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Chest")]
    public static new void Create()
    {
        var asset = CreateInstance<Chest>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Chest", "Items");
    }
#endif //UNITY_EDITOR
}
