﻿using UnityEngine;
using System.Collections;
using System;

public class CollectibleItem : ItemData {

    public SoundEffectManager.SoundEffect collectionSound;

    public override bool OnItemPickup(Item item)
    {
        PlayerInventory.instance.AddItem(this);
        SoundEffectManager.instance.PlaySound(collectionSound);
        return true;
    }

    public override bool OnItemUse(Item item)
    {
        Debug.Log("Silly you! " + name + " cannot be used.");
        return false;
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Collectible item")]
    public static void Create()
    {
        var asset = CreateInstance<CollectibleItem>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Collectible item", "Items");
    }
#endif //UNITY_EDITOR
}
