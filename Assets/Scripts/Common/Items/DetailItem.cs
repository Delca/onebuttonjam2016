﻿using UnityEngine;
using System.Collections;
using System;

public class DetailItem : ItemData
{
    public override bool OnItemPickup(Item item)
    {
        return false;   
    }

    public override bool OnItemUse(Item item)
    {
        return false;
    }
#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Detail item")]
    public static void Create()
    {
        var asset = CreateInstance<DetailItem>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Detail item", "Items");
    }
#endif //UNITY_EDITOR
}
