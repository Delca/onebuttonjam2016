﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ExitItem : DetailItem {

    public bool goToBossLevel = false;

    public override bool OnItemPickup(Item item)
    {
        if (OverworldManager.instance.isBossLevel)
        {
            OverworldManager.instance.isBossLevel = false;
            SceneManager.LoadScene("EndScreen");
            return false;
        }

        if (goToBossLevel)
        {
            OverworldManager.instance.GoToBossLevel();
        }
        else
        {
            OverworldManager.instance.GoToNextLevel();
        }

        return false;
    }
#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Exit item")]
    public static new void Create()
    {
        var asset = CreateInstance<ExitItem>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Exit item", "Items");
    }
#endif //UNITY_EDITOR
}
