﻿using UnityEngine;
using System.Collections;
using System;

public class GuardedItem : ItemData {

    public OverworldEnemyData guard;
    public ItemData[] loot;

    public override bool OnItemPickup(Item item)
    {
        throw new NotImplementedException();
    }

    public override bool OnItemUse(Item item)
    {
        throw new NotImplementedException();
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Guarded item")]
    public static void Create()
    {
        var asset = CreateInstance<GuardedItem>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Guarded item", "Items");
    }
#endif //UNITY_EDITOR
}
