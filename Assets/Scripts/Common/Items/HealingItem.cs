﻿using UnityEngine;
using System.Collections;
using System;

public class HealingItem : ImmediateUseItem {

    public int healingAmount = 10;

    HealingItem()
    {}

    public override bool OnItemUse(Item item)
    {
        foreach (Entity entity in PlayerInventory.instance.GetTargets())
        {
            if (entity.hp > 0)
            {
                if (entity.hp == entity.maxHp)
                {
                    entity.maxHp += Math.Max(1, healingAmount / 4);
                }
                entity.ApplyDamage(Element.Heal, healingAmount);
            }
        }
        PlayerMessage.Display(healingAmount.ToString() + " HP restored!");
        SoundEffectManager.instance.PlaySound(collectionSound);

        return true;
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Healing item")]
    public static new void Create()
    {
        var asset = CreateInstance<HealingItem>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Healing item", "Items");
    }
#endif //UNITY_EDITOR
}
