﻿using UnityEngine;
using System.Collections;

public class ImmediateUseItem : CollectibleItem {

    public override bool OnItemPickup(Item item)
    {
        PlayerInventory.instance.SetTarget(Player.player.entities.ToArray());
        bool usedUp = OnItemUse(item);
        PlayerInventory.instance.ClearTargets();
        return usedUp;
    }
}
