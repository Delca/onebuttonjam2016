﻿using UnityEngine;
using System.Collections;

public abstract class ItemData : ScriptableObject {

    public bool counterAlwaysDisplayed = false;
    public Sprite sprite;

    public abstract bool OnItemPickup(Item item);

    public abstract bool OnItemUse(Item item);
}
