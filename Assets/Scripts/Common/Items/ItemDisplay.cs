﻿using UnityEngine;
using UnityEngine.UI;

public class ItemDisplay : MonoBehaviour
{
    public ItemData itemTarget;
    public Text quantityLabel;
    int displayedQuantity = 0;
    
    void Update()
    {
        int currentQuantity = PlayerInventory.instance.GetItemQuantity(itemTarget);

        if (currentQuantity != displayedQuantity)
        {
            displayedQuantity = currentQuantity;
            quantityLabel.text = (displayedQuantity < 10 ? "0" : "") + displayedQuantity;
        }
    }
}
