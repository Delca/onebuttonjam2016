﻿using UnityEngine;
using System.Collections;
using System;

public class ManaRestoringItem : ImmediateUseItem {

    public int restoredAmount = 10;

    ManaRestoringItem()
    { }

    public override bool OnItemUse(Item item)
    {
        foreach (Entity entity in PlayerInventory.instance.GetTargets())
        {
            if (entity.mp == entity.maxMp)
            {
                entity.maxMp += Math.Max(1, restoredAmount / 3);
            }
            entity.RestoreMp(restoredAmount);
        }
        PlayerMessage.Display(restoredAmount.ToString() + " MP restored!");
        SoundEffectManager.instance.PlaySound(collectionSound);

        return true;
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Mana restoring item")]
    public static new void Create()
    {
        var asset = CreateInstance<ManaRestoringItem>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Mana restoring item", "Items");
    }
#endif //UNITY_EDITOR
}
