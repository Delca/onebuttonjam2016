﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInventory {
    private static PlayerInventory _instance = null;
    public static PlayerInventory instance {
        get
        {
            if (_instance == null)
            {
                _instance = new PlayerInventory();
            }

            return _instance;
        }
    }

    IDictionary<ItemData, int> items;
    IList<Entity> targets;

    private PlayerInventory()
    {
        items = new Dictionary<ItemData, int>();
        targets = new List<Entity>();
    }

    public void AddItem(ItemData item, int quantity = 1)
    {
        if (items.Keys.Contains(item))
        {
            items[item] += quantity;
        }
        else
        {
            items[item] = quantity;
        }
    }

    public void Remove(ItemData item, int quantity = 1)
    {
        if (items.Keys.Contains(item))
        {
            if (items[item] >= quantity)
            {
                items[item] -= quantity;
            }
            else
            {
                Debug.LogError("Not enough " + item.name + " in the inventory, removing only " + (quantity - items[item]));
                items[item] = 0;
            }
        }
        else
        {
            Debug.LogError("No " + item.name + " in the inventory, cannot remove any" );
        }
    }

    public ItemData GetItem<T>(T itemData)
        where T : ItemData
    {
        if (items.Keys.Contains(itemData) && items[itemData] > 0)
        {
            items[itemData] -= 1;
            return itemData;
        }

        return null;
    }

    public int GetItemQuantity<T>(T itemData)
        where T : ItemData
    {
        return (items.Keys.Contains(itemData) ? items[itemData] : 0);
    }

    public ICollection<ItemData> GetItems()
    {
        return items.Keys;
    }

    public ICollection<ItemData> GetItemDatas()
    {
        return items.Keys;
    }

    public IList<Entity> GetTargets()
    {
        return targets;
    }

    public void SetTarget(params Entity[] targets)
    {
        this.targets.Clear();
        foreach (Entity target in targets)
        {
            this.targets.Add(target);
        }
    }

    public void ClearTargets()
    {
        targets.Clear();
    }
}
