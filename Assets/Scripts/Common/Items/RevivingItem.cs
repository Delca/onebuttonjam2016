﻿using UnityEngine;
using System.Collections;

public class RevivingItem : ImmediateUseItem {

    public override bool OnItemUse(Item item)
    {
        bool isUsed = false;
        string logMessage = "";

        foreach (Entity entity in PlayerInventory.instance.GetTargets())
        {
            if (entity.hp <= 0)
            {
                isUsed = true;
                entity.ApplyDamage(Element.Heal, entity.maxHp);
                entity.RestoreMp(entity.maxMp - (int)entity.mp);

                logMessage += (logMessage.Length > 0 ? ", " : "") + entity.name;
            }
        }

        if (isUsed)
        {
            if (logMessage.IndexOf(",") != -1)
            {
                logMessage += " are revived!";
            }
            else
            {
                logMessage += " is revived!";
            }
            PlayerMessage.Display(logMessage);
            SoundEffectManager.instance.PlaySound(collectionSound);
        }        

        return isUsed;
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Reviving item")]
    public static new void Create()
    {
        var asset = CreateInstance<RevivingItem>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Reviving item", "Items");
    }
#endif //UNITY_EDITOR
}
