﻿using UnityEngine;
using System.Collections;

public class StatBoostItem : ImmediateUseItem {

    public enum Stat { ATTACK, DEFENSE, MAGIC, SPEED };

    public Stat statToBoost;
    public int boostAmount;
    public EntityData.EntityClass targetClass;

    static int genericHPBoost = 5;
    static int genericMPBoost = 5;

    public override bool OnItemUse(Item item)
    {
        foreach (Entity entity in PlayerInventory.instance.GetTargets())
        {
            if (entity.entityClass == targetClass)
            {
                switch (statToBoost)
                {
                    case Stat.ATTACK:
                        entity.attack += boostAmount;
                        break;
                    case Stat.DEFENSE:
                        entity.defense += boostAmount;
                        break;
                    case Stat.MAGIC:
                        entity.magic += boostAmount;
                        break;
                    default:
                    case Stat.SPEED:
                        entity.speed += boostAmount;
                        break;
                }

                entity.maxHp += genericHPBoost;
                entity.ApplyDamage(Element.Heal, genericHPBoost);
                entity.maxMp += genericMPBoost;
                entity.RestoreMp(genericMPBoost);

                PlayerMessage.Display(targetClass.ToString() + " " + statToBoost.ToString() + " increased!");
                SoundEffectManager.instance.PlaySound(collectionSound);
            }
        }        

        return true;
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Stat boost item")]
    public static new void Create()
    {
        var asset = CreateInstance<StatBoostItem>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Stat boost item", "Items");
    }
#endif //UNITY_EDITOR
}
