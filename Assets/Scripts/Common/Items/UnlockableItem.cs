﻿using System;

public class UnlockableItem : ItemData
{
    public ItemData keyToUnlock;

    public override bool OnItemPickup(Item item)
    {
        return true;
    }

    public override bool OnItemUse(Item item)
    {
        return false;
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Item/Unlockable item")]
    public static void Create()
    {
        var asset = CreateInstance<UnlockableItem>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Unlockable item", "Items");
    }
#endif //UNITY_EDITOR
}
