﻿using UnityEngine;
using System.Collections;

public class MusicData : ScriptableObject {

    public AudioClip music;
    public float introLength = 0;
    public float loopEnd = 0;

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Music track")]
    public static void Create()
    {
        var asset = CreateInstance<MusicData>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Music track", "Music");
    }
#endif //UNITY_EDITOR
}
