﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public enum MusicTrack { DUNGEON = 0, BATTLE, BOSSBATTLE };

    public MusicData[] tracks;
    AudioClip currentTrack = null;
    float introLength = 0;
    float loopEnd = 0;
    float loopLength = 0;
    double nextRestart = 0;
    bool usingFirstSource = true;
    double schedulingInterval = 1f;

    AudioSource firstSource;
    AudioSource secondSource;

    public static MusicManager instance;

    // Use this for initialization
    void Start()
    {
        if (instance != null)
        {
            Destroy(this);
            return;
        }

        instance = this;

        firstSource = gameObject.AddComponent<AudioSource>();
        secondSource = gameObject.AddComponent<AudioSource>();

        firstSource.playOnAwake = false;
        secondSource.playOnAwake = false;

        PlayTrack(MusicTrack.DUNGEON);
	}
	
    void PlayTrack(MusicData musicdata)
    {
        currentTrack = musicdata.music;

        firstSource.Stop();
        secondSource.Stop();

        firstSource.clip = currentTrack;
        firstSource.transform.parent = transform;
        secondSource.clip = currentTrack;
        secondSource.transform.parent = transform;

        if (loopEnd == 0)
        {
            loopEnd = currentTrack.length;
        }
        this.introLength = musicdata.introLength;
        this.loopEnd = (musicdata.loopEnd == 0 ? currentTrack.length : musicdata.loopEnd);
        loopLength = loopEnd - introLength;

        PlayIntro();
    }

    void PlayIntro()
    {
        double introEndTime = AudioSettings.dspTime + introLength;
        firstSource.Play();
        firstSource.SetScheduledEndTime(introEndTime);
        usingFirstSource = true;
        nextRestart = introEndTime;
    }

	// Update is called once per frame
	void Update () {
        AudioSource nextSource = (usingFirstSource ? secondSource : firstSource);
        
        double now = AudioSettings.dspTime;

        if (now > nextRestart - schedulingInterval)
        {
            usingFirstSource = !usingFirstSource;
            nextSource.time = introLength;
            nextSource.PlayScheduled(nextRestart);
            nextSource.SetScheduledEndTime(nextRestart + loopLength);
            nextRestart += loopLength;
        }
	}

    public void PlayTrack(MusicTrack index)
    {
        PlayTrack(tracks[(int)index]);
    }
}
