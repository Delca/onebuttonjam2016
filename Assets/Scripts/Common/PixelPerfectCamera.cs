﻿using UnityEngine;
using System.Collections;

public class PixelPerfectCamera : MonoBehaviour
{
    public const float defaultPixelsPerUnit = 100f;
    public const float defauteUnitsPerPixel = 1 / defaultPixelsPerUnit;

    public float pixelsPerUnit = defaultPixelsPerUnit;
    [HideInInspector]
    public float unitsPerPixel { get; private set; }

    public int width;
    public int height;

    private Camera _camera;

    // Use this for initialization
    void Start()
    {
        _camera = GetComponent<Camera>();
        unitsPerPixel = 1 / pixelsPerUnit;
        _camera.orthographicSize = Screen.height * .5f * unitsPerPixel;
    }

    // Update is called once per frame
    // Use this for initialization
    void Update()
    {
        if (_camera != null)
            _camera.orthographicSize = Screen.height * .5f * unitsPerPixel;
    }

}
