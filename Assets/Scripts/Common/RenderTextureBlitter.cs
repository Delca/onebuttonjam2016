﻿using UnityEngine;
using System.Collections;

public class RenderTextureBlitter : MonoBehaviour {

    public RenderTexture screenTexture;

    void OnPostRender()
    {
        Graphics.Blit(screenTexture, null as RenderTexture);
        
    }
}
