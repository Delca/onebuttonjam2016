﻿using System;
using System.Collections;
using UnityEngine;

public enum Element
{
    Physical,
    Fire,
    Ice,
    Thunder,
    Heal
}

public class Skill : ScriptableObject
{
    public int mpCost;
    public SkillPattern pattern;
    public Element element;

    public int basePower;
    public int randomPower;

    public Transform projectilePrefab;
    public SoundEffectManager.SoundEffect skillSoundEffect;

    public void Cast(Fighter caster, Fighter target, Action onTurnEndCallback)
    {
        var power = basePower + UnityEngine.Random.Range(0, randomPower + 1);
        if (element == Element.Physical)
            power += caster.entity.attack;
        else
            power += caster.entity.magic;

        if (element != Element.Heal)
        {
            power = Mathf.Max(0, power - target.entity.defense);
            Debug.LogFormat("{0} damaged {1} for {2} HP", caster.name, target.name, power);
        }
        else
            Debug.LogFormat("{0} healed {1} for {2} HP", caster.name, target.name, power);

        caster.entity.ConsummeMp(mpCost);

        Action onImpactCallback = () => { target.ApplyDamage(element, power); };
        if (projectilePrefab == null)
            caster.StartCoroutine(caster.BumpTarget(target, onImpactCallback, onTurnEndCallback));
        else
        {
            var projectile = Instantiate(projectilePrefab).GetComponent<Projectile>();
            projectile.Fire(caster, target, onImpactCallback, onTurnEndCallback);
        }

        if (SoundEffectManager.instance != null)
        {
            SoundEffectManager.instance.PlaySound(skillSoundEffect);
        }
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Skill")]
    public static void Create()
    {
        var asset = CreateInstance<Skill>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "New Skill", "Skills");
    }
#endif //UNITY_EDITOR
}
