﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SkillPattern
{
    public static readonly int PATTERN_LENGTH = 4;
    public bool[] buttonStates = new bool[PATTERN_LENGTH];

    public SkillPattern(List<bool> states)
    {
        Debug.Assert(states.Count == PATTERN_LENGTH);
        for (int i = 0; i < PATTERN_LENGTH; ++i)
            buttonStates[i] = states[i];
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        var pattern = obj as SkillPattern;
        return this == pattern;
    }

    public override int GetHashCode()
    {
        int hashCode = 0;
        for (int i = 0; i < buttonStates.Length; ++i)
        {
            if (buttonStates[i])
                hashCode += Mathf.FloorToInt(Mathf.Pow(2, i));
        }
        return hashCode;
    }

    public static bool operator ==(SkillPattern lhs, SkillPattern rhs)
    {
        if (((object)lhs == null) || ((object)rhs == null))
            return false;

        if (ReferenceEquals(lhs, rhs))
            return true;

        for (int i = 0; i < lhs.buttonStates.Length; ++i)
        {
            if (lhs.buttonStates[i] != rhs.buttonStates[i])
                return false;
        }
        return true;
    }

    public static bool operator !=(SkillPattern lhs, SkillPattern rhs)
    {
        return !(lhs == rhs);
    }
}
