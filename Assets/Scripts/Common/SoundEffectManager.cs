﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundEffectManager : MonoBehaviour {

    public enum SoundEffect { NONE = 0, CUT, HCUT, FIRE, ICE, SHOCK, HEAL, ACTIONMISS,
                              ENEMYDEATHHIGH, PLAYERDEATH, VICTORY, MANARESTORE, COLLECTKEY,
                              BUMP, BEEPLOW, BEEPHIGH, CHESTOPEN, STATBOOST};

    public AudioClip[] soundEffects;
    IList<AudioSource> audioSources;

    public static SoundEffectManager instance;

	// Use this for initialization
	void Start () {
        if (instance != null)
        {
            Destroy(this);
            return;
        }

        instance = this;

        audioSources = new List<AudioSource>();
	}
	
	public void PlaySound(SoundEffect index)
    {
        if (index == SoundEffect.NONE)
        {
            return;
        }

        AudioSource selectedSource = null;
        for (int i = 0; i < audioSources.Count; ++i)
        {
            if (!audioSources[i].isPlaying)
            {
                selectedSource = audioSources[i];
                break;
            }
        }

        if (selectedSource == null)
        {
            selectedSource = gameObject.AddComponent<AudioSource>();
            selectedSource.volume = 0.2f;
            audioSources.Add(selectedSource);
        }

        selectedSource.clip = soundEffects[(int)index - 1];
        selectedSource.Play();
    }
}
