﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    public Text playAgainLabel;
    public float blinkDelay = .5f;

    private Color textColor_;
    private float timer_;
    private bool isLoading_ = false;

    void Start()
    {
        textColor_ = playAgainLabel.color;
        timer_ = blinkDelay;
    }

    void Update()
    {
        if ((!isLoading_) && (Input.GetButtonDown("Button")))
        {
            SceneManager.LoadSceneAsync("Overworld");
            isLoading_ = true;
        }

        if (timer_ < 0)
        {
            playAgainLabel.color = (playAgainLabel.color == Color.clear) ? textColor_ : Color.clear;
            timer_ = blinkDelay;
        }
        else
            timer_ -= Time.deltaTime;
    }
}
