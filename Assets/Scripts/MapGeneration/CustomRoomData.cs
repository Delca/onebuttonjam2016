﻿using UnityEngine;
using System.Collections;

public class CustomRoomData : ScriptableObject {

    [TextArea]
    public string roomDesc;
    public ItemData[] items;
    public int limitPerLevel = 2;
    public bool allowSpawnAndExit = true;

    public int currentlyInLevel = 0;

    CustomRoomData()
    {}

    public int GetWidth()
    {
        return roomDesc.Split('\n')[0].Split(',').Length;
    }

    public int GetHeight()
    {
        return roomDesc.Split('\n').Length;
    }

    public TileInfo[,] getTileArray()
    {
        int width = GetWidth();
        int height = GetHeight();

        TileInfo[,] tileArray = new TileInfo[width, height];

        string[][] parsedDesc = new string[height][];

        int counter = 0;
        foreach (string line in roomDesc.Split('\n'))
        {
            parsedDesc[counter++] = line.Split(',');
        }

        Debug.Log("Getting room " + name);

        for (int j = 0; j < height; ++j)
            for (int i = 0; i < width; ++i)
            {
                //Debug.Log(i + " " + parsedDesc[j].Length);
                TileInfo tileInfo = new TileInfo(TileInfo.Type.RoomFloor);
                string tile = parsedDesc[j][i].TrimEnd().TrimStart();
                int itemIndex = -1;

                if (int.TryParse(tile, out itemIndex) && itemIndex < items.Length)
                {
                    tileInfo.content = items[itemIndex];
                }
                else if (tile == "h")
                {
                    tileInfo.type = TileInfo.Type.HardenedWall;
                }
                else if (tile == "w" || tile == "#")
                {
                    tileInfo.type = TileInfo.Type.CorridorWall;
                }
                else if (tile == "c")
                {
                    tileInfo.type = TileInfo.Type.CorridorFloor;
                }
                else if (tile == "?")
                {
                    tileInfo.content = items[Random.Range(0, items.Length)];
                }

                tileArray[i, j] = tileInfo;
            }

        return tileArray;
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Custom room")]
    public static void Create()
    {
        var asset = CreateInstance<CustomRoomData>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Custom room", "Rooms");
    }
#endif //UNITY_EDITOR
}
