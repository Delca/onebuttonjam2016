﻿using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using System;
using Position = AStar.Position;
using UnityEngine.SceneManagement;

public class DungeonLevelGenerator : MonoBehaviour
{
    public class LevelPart
    {
        public int x, y, width, height, depth, id;
        public IList<LevelPart> children;
        public CustomRoomData room;

        public static int depthThreshold = 2; // TODO: value should be 5~6
        public static float splitProbability = 0.5f;

        public int xCenter, yCenter;

        private static int nodeCounter;

        public LevelPart(int x, int y, int width, int height, int depth = 0, int id = -1, CustomRoomData room = null)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.depth = depth;
            this.room = room;
            if (id < 0)
                this.id = nodeCounter++;

            this.children = new List<LevelPart>();
        }

        public void VisitAfter(Action<LevelPart> action)
        {
            action(this);

            foreach (LevelPart child in children)
                child.VisitAfter(action);
        }

        public void VisitBefore(Action<LevelPart> action)
        {
            foreach (LevelPart child in children)
                child.VisitBefore(action);

            action(this);
        }

        public void RecursiveSplit(Action<LevelPart> split = null)
        {
            split = split ?? ((LevelPart b) =>
            {
                if ((b.depth > depthThreshold && Random.Range(0, 100) / 100f >= splitProbability))
                    return;

                b.SplitIntoTwo();
            });
            VisitAfter(split);
        }

        public void SplitIntoTwo()
        {
            bool horizontalCut = (height > width);

            int minWidth = 10;
            int minHeight = 10;

            if (horizontalCut && height < minHeight)
            {
                if (width < minWidth)
                {
                    return;
                }

                horizontalCut = false;
            }
            if (!horizontalCut && width < minWidth)
            {
                if (height < minHeight)
                {
                    return;
                }

                horizontalCut = true;
            }

            int spaceToCut = (horizontalCut ? height : width) - 4;
            int cutThreshold = 2 + spaceToCut / 2 + Random.Range(0, 2 * spaceToCut / 5);

            children.Clear();

            children.Add(new LevelPart(x, y,
                                     (horizontalCut ? width : cutThreshold),
                                     (horizontalCut ? cutThreshold : height),
                                     depth + 1
                                    )
                        );
            children.Add(new LevelPart((horizontalCut ? x : x + cutThreshold),
                                     (horizontalCut ? y + cutThreshold : y),
                                     (horizontalCut ? width : width - cutThreshold),
                                     (horizontalCut ? height - cutThreshold : height),
                                     depth + 1
                                    )
                        );
        }

        public List<LevelPart> AsList(List<LevelPart> nodes = null)
        {
            nodes = nodes ?? new List<LevelPart>();

            this.VisitAfter((LevelPart n) => {
                nodes.Add(n);
            });

            return nodes;
        }

        string indentToString(int indent = 0)
        {
            string log = new String(' ', indent * 2) + id + "(" + x + " " + y + " " + width + " " + height + ")\n";

            foreach (LevelPart child in children)
                log += child.indentToString(indent + 1);

            return log;
        }

        public override string ToString()
        {
            return indentToString();
        }

        public void debugShow()
        {
            Color c = Color.green;

            Debug.DrawLine(new Vector3(Constant.tileWidth * (x - 0.5f), Constant.tileHeight * (y - 0.5f), 1), new Vector3(Constant.tileWidth * (x + width - 0.5f), Constant.tileHeight * (y - 0.5f), 1), c, 6000, false);
            Debug.DrawLine(new Vector3(Constant.tileWidth * (x + width - 0.5f), Constant.tileHeight * (y - 0.5f), 1), new Vector3(Constant.tileWidth * (x + width - 0.5f), Constant.tileHeight * (y + height - 0.5f), 1), c, 6000, false);
            Debug.DrawLine(new Vector3(Constant.tileWidth * (x + width - 0.5f), Constant.tileHeight * (y + height - 0.5f), 1), new Vector3(Constant.tileWidth * (x - 0.5f), Constant.tileHeight * (y + height - 0.5f), 1), c, 6000, false);
            Debug.DrawLine(new Vector3(Constant.tileWidth * (x - 0.5f), Constant.tileHeight * (y + height - 0.5f), 1), new Vector3(Constant.tileWidth * (x - 0.5f), Constant.tileHeight * (y - 0.5f), 1), c, 6000, false);

            //Debug.DrawLine(new Vector3(Constant.tileHeight * (y - 0.5f), Constant.tileWidth*(x - 0.5f), 1), new Vector3(Constant.tileHeight * (y - 0.5f), Constant.tileWidth * (x + width - 0.5f), 1), c, 6000, false);
            //Debug.DrawLine(new Vector3(Constant.tileHeight * (y - 0.5f), Constant.tileWidth * (x + width - 0.5f), 1), new Vector3(Constant.tileHeight * (y + height - 0.5f), Constant.tileWidth * (x + width - 0.5f), 1), c, 6000, false);
            //Debug.DrawLine(new Vector3(Constant.tileHeight * (y + height - 0.5f), Constant.tileWidth * (x + width - 0.5f), 1), new Vector3(Constant.tileHeight * (y + height - 0.5f), Constant.tileWidth * (x - 0.5f), 1), c, 6000, false);
            //Debug.DrawLine(new Vector3(Constant.tileHeight * (y + height - 0.5f), Constant.tileWidth * (x - 0.5f), 1), new Vector3(Constant.tileHeight * (y - 0.5f), Constant.tileWidth * (x - 0.5f), 1), c, 6000, false);
        }

        public void updateCenter(TileInfo[,] tiles)
        {
            int counter = 0;
            float xCenter = 0;
            float yCenter = 0;

            for (int i = y; i < y + height; ++i)
                for (int j = x; j < x + width; ++j)
                    if (tiles[i, j].type == TileInfo.Type.RoomFloor || tiles[i, j].type == TileInfo.Type.CorridorFloor)
                    {
                        ++counter;
                        xCenter += i;
                        yCenter += j;
                    }

            if (counter <= 0)
            {
                this.xCenter = x + width / 2;
                this.yCenter = y + height / 2;
                return;
            }

            xCenter /= counter;
            yCenter /= counter;
            
            if (tiles[(int) xCenter, (int) yCenter].type != TileInfo.Type.RoomFloor &&
                tiles[(int) xCenter, (int) yCenter].type != TileInfo.Type.CorridorFloor)
            {
                int nearestX = (int) xCenter;
                int nearestY = (int) yCenter;
                int nearestDistance = Int32.MaxValue;

                for (int j = y; j < y + height; ++j)
                    for (int i = x; i < x + width; ++i)
                    {
                        if ((tiles[i, j].type == TileInfo.Type.RoomFloor || tiles[i, j].type == TileInfo.Type.CorridorFloor) &&
                            Math.Abs((int)xCenter - i) + Math.Abs((int)yCenter - j) < nearestDistance)
                        {
                            nearestDistance = Math.Abs((int)xCenter - i) + Math.Abs((int)yCenter - j);
                            nearestX = i;
                            nearestY = j;
                            
                            if (nearestDistance == 0)
                            {
                                break;
                            }
                        }
                        if (nearestDistance == 0)
                        {
                            break;
                        }
                    }

                xCenter = nearestX;
                yCenter = nearestY;
            }

            this.xCenter = (int) xCenter;
            this.yCenter = (int) yCenter;
            
        }

        public int distanceBetweenCenters(LevelPart target, TileInfo[,] tiles, bool doNotSelfUpdate = false)
        {
            if (doNotSelfUpdate)
            {
                updateCenter(tiles);
            }
            target.updateCenter(tiles);

            return Math.Abs(xCenter - target.xCenter) + Math.Abs(yCenter - target.yCenter);
        }

        public Position selectRandomTile(TileInfo.Type tileType, TileInfo[,] tiles)
        {
            IList<Position> validTiles = new List<Position>();

            for (int j = y; j < y + height; ++j)
                for (int i = x; i < x + width; ++i)
                    if (tiles[i, j].type == tileType)
                    {
                        validTiles.Add(new Position(i, j));
                    }

           return (validTiles.Count <= 0 ? new Position(xCenter, yCenter) : validTiles[Random.Range(0, validTiles.Count)]);
        }
    };

    public int seed = -1;
    public Sprite[] tileset;
    public ItemData[] itemsDatabase;
    public CustomRoomData[] customRooms;
    public ItemData[] enemies;
    public GameObject tilePrefab;

    public ItemData doorData;
    public ItemData doorKeyData;

    [HideInInspector]
    public Position spawnPoint;
    public ItemData spawnPointData;
    [HideInInspector]
    public Position exitPoint;
    public ItemData exitPointData;

    private GameObject levelContainer;
    private TileInfo[,] levelTiles;
    private int[,] subTiles;
    private IDictionary<TileInfo.Type, IList<TileInfo>> tilesByType;
    private int rowNumber = 20;
    private int columnNumber = 20;

    private int cutoutDepthThreshold = 5;
    private float customRoomProbability = 0.8f;
    private float randomRoomProbability = 0.5f;
    private float spawnProbability = 0.05f;

    // Use this for initialization
    void Start ()
	{
        if (seed != -1)
        {
            Random.InitState(seed);
        }
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

    public void loadGenerationTemplate(GenerationTemplateData gTD)
    {
        seed = gTD.seed;
        if (seed != -1)
        {
            Random.InitState(seed);
        }
        else
        {
            Random.InitState((int) (Time.realtimeSinceStartup * 345 - Time.realtimeSinceStartup * 6));
        }

        this.tileset = gTD.tileset;
        this.customRooms = gTD.customRooms;
        this.enemies = gTD.enemies;
        
        this.doorData = gTD.doorData;
        this.doorKeyData = gTD.doorKeyData;

        this.cutoutDepthThreshold = gTD.cutoutDepthThreshold;
        LevelPart.splitProbability = gTD.splitProbability;
        this.customRoomProbability = gTD.customRoomProbability;
        this.randomRoomProbability = gTD.randomRoomProbability;
        this.spawnProbability = gTD.spawnProbability;

        foreach (CustomRoomData customRoom in this.customRooms)
        {
            customRoom.currentlyInLevel = 0;
        }
    }

    public void generateLevel(GenerationTemplateData gTD, int tries = 0) {
        if (tries > 5)
        {
            Debug.Log("Could not generate a level with those constraints");
            SceneManager.LoadScene("Title");
            return;
        }

        loadGenerationTemplate(gTD);

        int width = gTD.width, height = gTD.height, borderSize = 5;

        // Prepare the level generation
        rowNumber = height + 2 * borderSize;
        columnNumber = width + 2 * borderSize;                

        levelTiles = new TileInfo[columnNumber, rowNumber];
        tilesByType = new Dictionary<TileInfo.Type, IList<TileInfo>>();

        foreach (TileInfo.Type tileType in Enum.GetValues(typeof(TileInfo.Type)))
        {
            tilesByType[tileType] = new List<TileInfo>();
        }

        for (int i = 0; i < columnNumber; ++i)
            for (int j = 0; j < rowNumber; ++j)
                SetTile(i, j, TileInfo.Type.CorridorWall, 0);

        // Split the level into smaller parts
        LevelPart bspRoot = new LevelPart(borderSize, borderSize, width, height);
        bspRoot.RecursiveSplit((LevelPart b) =>
        {
            if ((b.depth > cutoutDepthThreshold && Random.Range(0, 100) / 100f >= LevelPart.splitProbability))
                return;

            b.SplitIntoTwo();
        });
        
        // Create rooms inside those parts
        IList<LevelPart> rooms = new List<LevelPart>();

        bspRoot.VisitAfter((LevelPart n) =>
        {
            if (n.children.Count == 0)
            {
                LevelPart customRoom = generateCustomRoom(n);

                if (customRoom != null && Random.Range(0, 100) / 100f <= customRoomProbability)
                {
                    ++customRoom.room.currentlyInLevel;
                    rooms.Add(customRoom);
                }
                //else if (Random.Range(0, 100) <= ((n.width * n.height * Mathf.Pow(2, n.depth - 2) * 1f) / (columnNumber * rowNumber)) * 100)
                else if (Random.Range(0, 100) / 100f <= randomRoomProbability)
                    rooms.Add(this.generateRandomRoom(n));
            }
        });

        if (rooms.Count > 1)
        {
            foreach (LevelPart room in rooms)
            {
                paintLevelPartAsRoom(room);
            }

            // Link those rooms together
            ensureConnectivity();

            // Place the exit and spawnpoint
            if (!placeSpawnAndExitPoints(rooms))
            {
                generateLevel(gTD, ++tries);
            }

            if (!OverworldManager.instance.isBossLevel)
            {
                placeDoorAndKey(spawnPoint, exitPoint);
            }
        }
        else
        {
            generateLevel(gTD, ++tries);
        }           
    }

    public void instantiateLevel(bool debugTreeStruct = false)
    {
        levelContainer = new GameObject("Level container");
        levelContainer.transform.parent = transform;
        levelContainer.transform.position = new Vector3(columnNumber * Constant.tileWidth, rowNumber * Constant.tileHeight, 0);

        applyTileSet();
        StitchedTileManager.instance.SetTileSet(tileset);

        for (int i = 0; i < columnNumber; ++i)
            for (int j = 0; j < rowNumber; ++j)
            {
                TileInfo tileInfo = levelTiles[i, j];
               
                GameObject tileGameObject = Instantiate(tilePrefab, new Vector3(Constant.tileWidth * i, Constant.tileHeight * j, 0), Quaternion.identity) as GameObject;
                Tile tile = tileGameObject.GetComponent<Tile>();
                tile.tileInfo = tileInfo;
                tile.x = i;
                tile.y = j;
                tileGameObject.GetComponent<Tile>().tileInfo = tileInfo;
                tileGameObject.GetComponent<SpriteRenderer>().sprite = getSpriteForTile(i, j);
                tileGameObject.name = "Tile@[" + i + "," + j + "]";
                tileGameObject.transform.SetParent(levelContainer.transform);

                if (tileInfo.type == TileInfo.Type.CorridorWall || tileInfo.type == TileInfo.Type.RoomWall || tileInfo.type == TileInfo.Type.HardenedWall)
                {
                    tileGameObject.GetComponent<BoxCollider2D>().isTrigger = false;
                    //tileGameObject.layer = LayerMask.NameToLayer("BlockingLayer");
                    tileGameObject.tag = "Wall";
                }

                if (tileInfo.content != null && (tileInfo.type == TileInfo.Type.RoomFloor || tileInfo.type == TileInfo.Type.CorridorFloor))
                {
                    OverworldManager.instance.SpawnItem(i, j, tileInfo.content);
                }
            }
    }

    public void Clear()
    {
        if (levelContainer != null)
        {
            Destroy(levelContainer);
        }
        levelContainer = null;
        levelTiles = null;
        subTiles = null;
        tilesByType = null;
    }

    void placeDoorAndKey(Position from, Position to)
    {
        AStar aStar = new AStar(levelTiles, columnNumber, rowNumber);

        IList<Position> pathToExit = aStar.getPath(from, to);
        TileInfo defaultTile = new TileInfo(TileInfo.Type.CorridorWall);

        int keyCounter = 0;

        
        while (pathToExit.Count > 0)
        {
            Position lastCorridorTile = null;

            for (int i = pathToExit.Count - 1; i >= 0; --i)
            {
                lastCorridorTile = pathToExit[i];
                if (getTileAt(lastCorridorTile.x, lastCorridorTile.y, defaultTile).type == TileInfo.Type.CorridorFloor)
                {
                    break;
                }
            }

            TileInfo tileInfo = getTileAt(lastCorridorTile.x, lastCorridorTile.y, null);
            tileInfo.content = doorData;
            ++keyCounter;

            // And once again ...
            aStar = new AStar(levelTiles, columnNumber, rowNumber);
            pathToExit = aStar.getPath(from, to);
        }

        Dictionary<int, List<Position>> validKeyTiles = new Dictionary<int, List<AStar.Position>>();
        floodTileAt(from.x, from.y, 0, validKeyTiles, new List<string>());

        //Debug.Log("Placed " + keyCounter + " doors");

        while (keyCounter > 0)
        {
            Position keyPosition = null;
            TileInfo keyTileInfo = null;

            int keyIte = 0;

            do
            {
                keyPosition = validKeyTiles[0][Random.Range(0, validKeyTiles[0].Count)];
                keyTileInfo = getTileAt(keyPosition.x, keyPosition.y, null);

                if (keyTileInfo.content == null || keyIte > 20)
                {
                    keyTileInfo.content = doorKeyData;
                }

                ++keyIte;
            } while ((keyTileInfo == null || keyTileInfo.content != doorKeyData));

            --keyCounter;
        }

    }

    bool placeSpawnAndExitPoints(IList<LevelPart> rooms)
    {
        LevelPart exitRoom = rooms[Random.Range(0, rooms.Count)];

        int tries = 0;
        while (tries < 2 * rooms.Count && (exitRoom == null || (exitRoom.room != null && !exitRoom.room.allowSpawnAndExit)))
        {
            exitRoom = rooms[Random.Range(0, rooms.Count)];
            ++tries;
        }

        if (tries >= 2 * rooms.Count)
        {
            return false;
        }

        int distanceToFarthestRoom = 0;
        LevelPart spawnRoom = null;

        exitRoom.updateCenter(levelTiles);

        foreach (LevelPart room in rooms)
        {
            if (room.room != null && !room.room.allowSpawnAndExit)
            {
                continue;
            }

            room.updateCenter(levelTiles);

            int distanceToRoom = exitRoom.distanceBetweenCenters(room, levelTiles, true);

            if ((distanceToRoom > distanceToFarthestRoom || spawnRoom == null))
            {
                distanceToFarthestRoom = distanceToRoom;
                spawnRoom = room;
            }
        }

        tries = 0;

        do
        {
            spawnPoint = spawnRoom.selectRandomTile(TileInfo.Type.RoomFloor, levelTiles);
        } while (getTileAt(spawnPoint.x, spawnPoint.y, null).content != null || tries++ < 50);
        getTileAt(spawnPoint.x, spawnPoint.y, null).content = spawnPointData;

        tries = 0;

        if (!OverworldManager.instance.isBossLevel)
        {
            do
            {
                exitPoint = exitRoom.selectRandomTile(TileInfo.Type.RoomFloor, levelTiles);
            } while (getTileAt(exitPoint.x, exitPoint.y, null).content != null || tries++ < 50);

            getTileAt(exitPoint.x, exitPoint.y, null).content = exitPointData;
        }

        return true;
    }

    void connectPoints(int xS, int yS, int xE, int yE)
    {
        AStar astar = new AStar(levelTiles, columnNumber, rowNumber, (TileInfo t) => { return t.type == TileInfo.Type.HardenedWall; });

        IList<AStar.Position> path = astar.getPath(new AStar.Position(xS, yS), new AStar.Position(xE, yE));

        foreach (AStar.Position pathWay in path)
        {
            TileInfo tile = new TileInfo(TileInfo.Type.CorridorFloor, 1);

            if (levelTiles[pathWay.x, pathWay.y].type == TileInfo.Type.RoomFloor ||
                levelTiles[pathWay.x, pathWay.y].type == TileInfo.Type.RoomWall)
            {
                tile.type = TileInfo.Type.RoomFloor;
            }

            if (enemies.Length > 0 && Random.Range(0, 100) / 100f < spawnProbability)
            {
                levelTiles[pathWay.x, pathWay.y].content = enemies[Random.Range(0, enemies.Length)];
            }

            SetTile(pathWay.x, pathWay.y, tile.type, tile.variation, levelTiles[pathWay.x, pathWay.y].content);
        }
    }

    void floodTileAt(int x, int y, int counter, Dictionary<int, List<Position>> connexComponent, List<string> visitedTiles)
    {
        Stack<Position> toVisit = new Stack<Position>();
        TileInfo defaultTileInfo = new TileInfo(TileInfo.Type.HardenedWall);

        int currentComponent = counter;
        connexComponent[currentComponent] = new List<Position>();
        toVisit.Clear();

        toVisit.Push(new Position(x, y));

        while (toVisit.Count > 0)
        {
            Position pos = toVisit.Pop();
            TileInfo t = getTileAt(pos.x, pos.y, defaultTileInfo);

            if (!visitedTiles.Contains(pos.x + "-" + pos.y) && t.type != TileInfo.Type.CorridorWall
                                                            && t.type != TileInfo.Type.RoomWall
                                                            && t.type != TileInfo.Type.HardenedWall
                                                            && !(t.content is UnlockableItem && (t.content as UnlockableItem).keyToUnlock != null))
            {
                connexComponent[currentComponent].Add(pos);

                toVisit.Push(new Position(pos.x - 1, pos.y));
                toVisit.Push(new Position(pos.x + 1, pos.y));
                toVisit.Push(new Position(pos.x, pos.y - 1));
                toVisit.Push(new Position(pos.x, pos.y + 1));
            }

            visitedTiles.Add(pos.x + "-" + pos.y);
        }

        if (connexComponent[currentComponent].Count <= 1)
        {
            Debug.Log("HI");
        }

    }

    void ensureConnectivity()
    {
        Dictionary<int, List<Position>> connexComponent = new Dictionary<int, List<Position>>();
        int counter = 0;
        List<string> visitedTiles = new List<string>();
        
        for (int i = 0; i < columnNumber; ++i)
            for (int j = 0; j < rowNumber; ++j)
            {
                TileInfo t = getTileAt(i, j, null);
                if (!visitedTiles.Contains(i + "-"+ j) && t.type != TileInfo.Type.CorridorWall && t.type != TileInfo.Type.RoomWall && t.type != TileInfo.Type.HardenedWall && !(t.content is UnlockableItem && (t.content as UnlockableItem).keyToUnlock != null))
                {
                    floodTileAt(i, j, counter++, connexComponent, visitedTiles);
                }
            }

        //Debug.Log("Component count" + connexComponent.Keys.Count);

        List<int> remainingKeys = new List<int>(connexComponent.Keys);

        int firstComponent = remainingKeys[Random.Range(0, remainingKeys.Count)];
        int secondComponent = firstComponent;

        while (remainingKeys.Count > 1)
        {
            while (secondComponent == firstComponent)
            {
                secondComponent = remainingKeys[Random.Range(0, remainingKeys.Count)];
            }

            Position startPoint = connexComponent[firstComponent][Random.Range(0, connexComponent[firstComponent].Count)];

            if (connexComponent[secondComponent].Count <= 1)
                Debug.Log("HI");

            Position endPoint = connexComponent[secondComponent][Random.Range(0, connexComponent[secondComponent].Count)];
            
            connectPoints(startPoint.x, startPoint.y, endPoint.x, endPoint.y);

            remainingKeys.Remove(firstComponent);
            firstComponent = secondComponent;
        }

    }

    Sprite getSpriteForTile(int i, int j)
    {
        if (i < 0 || j < 0 || i >= columnNumber || j >= rowNumber)
        {
            return null;
        }

        return StitchedTileManager.instance.GetTile(getSubTileAt(2 * i, 2 * j + 1, 0), getSubTileAt(2 * i + 1, 2 * j + 1, 0),
                                                    getSubTileAt(2 * i, 2 * j, 0), getSubTileAt(2 * i + 1, 2 * j, 0));
    }

    int getVariationForPattern(int[] pattern)
    {
        int w = -1; // Yeah, whatever...
        Func<int[], int[], bool> patternMatch = (int[] model, int[] value) =>
        {
            for (int i = 0; i < pattern.Length; ++i)
            {
                if (model[i] != w && model[i] != value[i])
                {
                    return false;
                }
            }

            return true;
        };
        
        // Simple wall patterns
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, w, w, w,
                                    w, 1, 1, 1, w,
                                    w, w, 0, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 8;
        }

        // ---- ---- //
        // 31 and 32 shoould have precedence over 21
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 0, w, w,
                                    w, w, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, w, 1, 0, w}, pattern))
        {
            return 30;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 0, w, w,
                                    w, w, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, 0, 1, w, w}, pattern))
        {
            return 31;
        }
        // 21 should take precedence over 7
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 0, w, w,
                                    w, w, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, w, 0, w, w}, pattern))
        {
            return 21;
        }


        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 0, w, w,
                                    w, 1, 1, 1, w,
                                    w, w, w, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 7;
        }
        // ---- ---- //

        // ---- ---- //
        // 11 and 12 shoud take precedence over 9 and 10
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 0, w, w,
                                    w, 0, 1, 1, w,
                                    w, 0, 1, 1, w,
                                    w, w, 0, w, w}, pattern))
        {
            return 22;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 0, w, w,
                                    w, 1, 1, 0, w,
                                    w, 1, 1, 0, w,
                                    w, w, 0, w, w}, pattern))
        {
            return 23;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, w, w, w,
                                    w, w, 1, 1, w,
                                    w, 0, 1, 1, w,
                                    w, w, 0, w, w}, pattern))
        {
            return 11;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, w, w, w,
                                    w, 1, 1, w, w,
                                    w, 1, 1, 0, w,
                                    w, w, 0, w, w}, pattern))
        {
            return 12;
        }
        // ---- ---- //


        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 1, w, w,
                                    w, 0, 1, w, w,
                                    w, w, 1, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 9;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 1, w, w,
                                    w, w, 1, 0, w,
                                    w, w, 1, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 10;
        }

        if (patternMatch(new int[] {w, w, 0, w, w,
                                    w, w, 1, w, w,
                                    w, 1, 1, w, w,
                                    w, 0, 1, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 9;
        }
        if (patternMatch(new int[] {w, w, 0, w, w,
                                    w, w, 1, w, w,
                                    w, w, 1, 1, w,
                                    w, w, 1, 0, w,
                                    w, w, w, w, w}, pattern))
        {
            return 10;
        }

        // Corners
        if (patternMatch(new int[] {w, w, 1, w, w,
                                    w, w, 1, 1, w,
                                    w, 0, 1, 1, w,
                                    w, w, 0, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 13;
        }
        if (patternMatch(new int[] {w, w, 1, w, w,
                                    w, 1, 1, w, w,
                                    w, 1, 1, 0, w,
                                    w, w, 0, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 14;
        }
        if (patternMatch(new int[] {w, w, 0, w, w,
                                    w, 0, 1, 1, w,
                                    w, 0, 1, 1, w,
                                    w, w, 0, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 24;
        }
        if (patternMatch(new int[] {w, w, 0, w, w,
                                    w, 1, 1, 0, w,
                                    w, 1, 1, 0, w,
                                    w, w, 0, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 25;
        }

        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 0, w, w,
                                    w, 0, 1, 1, w,
                                    w, w, 1, 1, w,
                                    w, w, w, w, w}, pattern))
        {
            return 15;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 0, w, w,
                                    w, 1, 1, 0, w,
                                    w, 1, 1, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 16;
        }

        if (patternMatch(new int[] {w, w, 1, w, w,
                                    w, w, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, w, 1, 0, w,
                                    w, w, w, w, w}, pattern))
        {
            return 10;
        }
        if (patternMatch(new int[] {w, w, 1, w, w,
                                    w, w, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, 0, 1, w, w,
                                    w, w, w, w, w}, pattern))
        {
            return 9;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 1, 0, w,
                                    w, 1, 1, 1, w,
                                    w, w, 1, 1, w,
                                    w, w, 1, 1, w}, pattern))
        {
            return 19;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, 0, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, 1, 1, w, w,
                                    w, 1, 1, w, w}, pattern))
        {
            return 20;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 1, 0, w,
                                    w, 1, 1, 1, w,
                                    w, w, 1, 1, w,
                                    w, w, 1, 0, w}, pattern))
        {
            return 29;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, 0, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, 1, 1, w, w,
                                    w, 0, 1, w, w}, pattern))
        {
            return 28;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 1, 0, w,
                                    w, 1, 1, 1, w,
                                    w, w, 1, w, w,
                                    w, w, 0, w, w}, pattern))
        {
            return 27;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, 0, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, w, 1, w, w,
                                    w, w, 0, w, w}, pattern))
        {
            return 26;
        }


        // Pattern targeting only the 5x5 environment
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, 1, w, w,
                                    w, w, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, w, 0, w, w}, pattern))
        {
            return 6;
        }
        
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, w, w, w,
                                    w, w, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, w, 1, 0, w}, pattern))
        {
            return 17;
        }
        if (patternMatch(new int[] {w, w, w, w, w,
                                    w, w, w, w, w,
                                    w, w, 1, w, w,
                                    w, 1, 1, 1, w,
                                    w, 0, 1, w, w}, pattern))
        {
            return 18;
        }
        



        // Default
        return pattern[(pattern.Length + 1)/2 - 1];
    }

    void applyTileSet()
    {
        generateSubTiles();

        int environmentSize = 2;
        int totalSize = 1 + 2 * environmentSize;

        applyRuleToSubTiles((int[,] environment) =>
        {
            int[] pattern = new int[totalSize * totalSize];
            int count = 0;

            for (int j = totalSize - 1; j >= 0; --j)
                for (int i = 0; i < totalSize; ++i)
                {
                    pattern[count++] = environment[i, j];
                }

            return getVariationForPattern(pattern);
        }, environmentSize);
    }

    void generateSubTiles()
    {
        subTiles = new int[2*columnNumber, 2*rowNumber];

        for (int i = 0; i < columnNumber; ++i)
            for (int j = 0; j < rowNumber; ++j)
            {
                int subTile = (levelTiles[i, j].type == TileInfo.Type.CorridorWall ||
                    levelTiles[i, j].type == TileInfo.Type.RoomWall || levelTiles[i, j].type == TileInfo.Type.HardenedWall) ? 1 : 0;

                subTiles[2 * i, 2 * j] = subTile;
                subTiles[2 * i + 1, 2 * j] = subTile;
                subTiles[2 * i, 2 * j  +1] = subTile;
                subTiles[2 * i + 1, 2 * j + 1] = subTile;
            }

    }

    LevelPart generateRandomRoom(LevelPart area = null)
    {
        area = area ?? new LevelPart(0, 0, columnNumber, rowNumber);

        int horizontalMargin = 1;
        int verticalMargin = 1;

        if (area.width < 16) horizontalMargin = 0;
        if (area.height < 16) verticalMargin = 0;

        int x, y, width = 0, height = 0;
        width = Math.Max(Random.Range(60, 100) * (area.width - 2 * horizontalMargin) / 100, 3);
        height = Math.Max(Random.Range(60, 100) * (area.height - 2 * verticalMargin) / 100, 3);
        
        x = horizontalMargin + Random.Range(0, (area.width - 2 * horizontalMargin) - width - 1);
        y = verticalMargin + Random.Range(0, (area.height - 2 * verticalMargin) - height - 1);
                
        if ((area.width <= 7 && Random.Range(0, 100) >= 65) || area.width <= 5)
        {
            width = area.width;
            x = 0;
        }
        if ((area.height <= 7 && Random.Range(0, 100) >= 65) || area.height < 5)
        {
            height = area.height;
            y = 0;
        }
        
        return new LevelPart(area.x + x, area.y + y, width, height, area.depth, area.id);
    }

    LevelPart generateCustomRoom(LevelPart area = null)
    {
        area = area ?? new LevelPart(0, 0, columnNumber, rowNumber);
        
        int x, y, width = 0, height = 0;

        int horizontalMargin = 1;
        int verticalMargin = 1;

        CustomRoomData selectedRoom = null;

        List<CustomRoomData> potentialRooms = new List<CustomRoomData>();

        foreach (CustomRoomData room in customRooms)
        {
            if (room.currentlyInLevel >= room.limitPerLevel)
            {
                continue;
            }

            height = room.GetHeight();
            if (height >= area.height - 2 * horizontalMargin) continue;
            width = room.GetWidth();
            if (width >= area.width - 2 * verticalMargin) continue;

            potentialRooms.Add(room);
            break;
        }

        if (potentialRooms.Count > 0)
        {
            selectedRoom = potentialRooms[Random.Range(0, potentialRooms.Count)];
            height = selectedRoom.GetHeight();
            width = selectedRoom.GetWidth();
        }
        else
        {
            return null;
        }

        x = Random.Range(0, (area.width - 2 * horizontalMargin) - width);
        y = Random.Range(0, (area.height - 2 * verticalMargin) - height);
        
        return new LevelPart(area.x + x, area.y + y, width, height, area.depth, area.id, selectedRoom);
    }

    void paintLevelPartAsRoom(LevelPart room)
    {
        if (room.room == null)
        {
            for (int i = room.y; i < room.y + room.height; ++i)
                for (int j = room.x; j < room.x + room.width; ++j)
                {
                    if (i == room.y || i == room.y + room.height - 1 ||
                        j == room.x || j == room.x + room.width - 1)
                    {
                        TileInfo.Type tileType = getTileAt(i, j, null).type;
                        if (tileType != TileInfo.Type.RoomFloor &&
                            tileType != TileInfo.Type.CorridorFloor &&
                            tileType != TileInfo.Type.HardenedWall)
                        {
                            SetTile(i, j, TileInfo.Type.RoomWall, 0);
                        }
                    }
                    else
                    {
                        ItemData content = null;
                        if (enemies.Length > 0 && Random.Range(0, 100) / 100f < spawnProbability)
                        {
                            content = enemies[Random.Range(0, enemies.Length)];
                        }

                        if (getTileAt(i, j, null).type != TileInfo.Type.HardenedWall)
                        {
                            SetTile(i, j, TileInfo.Type.RoomFloor, 1, content);
                        }
                    }                        
                }
                    
            return;
        }

        TileInfo[,] roomTiles = room.room.getTileArray();

        for (int i = 0; i < room.width; ++i)
            for (int j = 0; j < room.height; ++j)
            {
                SetTile(room.x + i, room.y + (room.height - 1 - j), roomTiles[i, j].type, roomTiles[i, j].variation, roomTiles[i, j].content);
            }
    }

    public TileInfo getTileAt(int i, int j, TileInfo defaultTile)
    {
        return (i >= 0 && j >= 0 &&
                i < columnNumber && j < columnNumber) ?
               levelTiles[i, j] :
               defaultTile;
    }

    int getSubTileAt(int i, int j, int defaultSubTile)
    {
        return (i >= 0 && j >= 0 &&
                i < 2 * columnNumber && j < 2 * columnNumber) ?
               subTiles[i, j] :
               defaultSubTile;
    }

    void applyRuleToTiles(Func<TileInfo[,], TileInfo> rule)
    {
        TileInfo[,] updatedLevelTiles = new TileInfo[columnNumber, rowNumber];
        TileInfo[,] environment = new TileInfo[3, 3];
        TileInfo defaultTile = new TileInfo(TileInfo.Type.CorridorWall);

        for (int i = 0; i < columnNumber; ++i)
            for (int j = 0; j < rowNumber; ++j)
            {
                for (int k = -1; k < 2; ++k)
                    for (int l = -1; l < 2; ++l)
                        environment[k + 1, l + 1] = getTileAt(i + k, j + l, defaultTile);

                        updatedLevelTiles[i, j] = rule(environment);
            }

        levelTiles = updatedLevelTiles;

        tilesByType = new Dictionary<TileInfo.Type, IList<TileInfo>>();
        foreach (TileInfo.Type tileType in Enum.GetValues(typeof(TileInfo.Type)))
        {
            tilesByType[tileType] = new List<TileInfo>();
        }
        foreach (TileInfo tile in levelTiles)
        {
            tilesByType[tile.type].Add(tile);
        }
    }

    void applyRuleToSubTiles(Func<int[,], int> rule, int environmentSize = 1)
    {
        int defaultSubtile = 1;

        int[,] updatedSubTiles = new int[2 * columnNumber, 2 * rowNumber];
        int[,] environment = new int[1 + 2 * environmentSize, 1 + 2 * environmentSize];        
        
        for (int i = 0; i < 2 * columnNumber; ++i)
            for (int j = 0; j < 2 * rowNumber; ++j)
            {
                for (int k = -environmentSize; k < environmentSize + 1; ++k)
                    for (int l = -environmentSize; l < environmentSize + 1; ++l)
                        environment[k + environmentSize, l + environmentSize] = getSubTileAt(i + k, j + l, defaultSubtile);

                updatedSubTiles[i, j] = rule(environment);
            }

        subTiles = updatedSubTiles;
    }

    public AStar getAStar(Func<TileInfo, bool> isWall = null)
    {
        return new AStar(levelTiles, columnNumber, rowNumber, isWall);
    }
    
    void debugLog()
    {
        string log = "";

        for (int i = 0; i < columnNumber; ++i)
        {
            for (int j = 0; j < rowNumber; ++j)
                log += levelTiles[i, j];

            log += '\n';
        }

        Debug.Log(log);
    }

    void debugShowTreeStruct(LevelPart node)
    {
        node.VisitAfter((LevelPart n) => {
            n.debugShow();
        });
    }

    public void placeRandomItems(TileInfo.Type floorType, ItemData item, int quantity = 1)
    {
        int failedAttempts = 0;
        while (quantity > 0 && failedAttempts < 64)
        {
            TileInfo spawnTile = tilesByType[floorType][Random.Range(0, tilesByType[floorType].Count - 1)];

            if (spawnTile.content != null)
                continue;

            spawnTile.content = item;
            --quantity;
        }

        if (failedAttempts >= 64)
        {
            Debug.LogError("Could not place the remaining " + quantity + " item of type " + item);
        }
    }

    public void SetTile(int x, int y, TileInfo.Type floorType, int variation = 0, ItemData content = null)
    {
        if (!(x >= 0 && y >= 0 &&
                x < columnNumber && y < columnNumber))
        {
            Debug.Log("Cannot place tile at " + x + ", "+ y);
            return;
        }

        TileInfo previousTile = levelTiles[x, y];
        if (previousTile != null)
        {
            tilesByType[previousTile.type].Remove(previousTile);
        }

        levelTiles[x, y] = new TileInfo(floorType, variation, (content != null ? content : (levelTiles[x, y] != null ? levelTiles[x, y].content : null)));
        tilesByType[floorType].Add(levelTiles[x, y]);
    }
}
