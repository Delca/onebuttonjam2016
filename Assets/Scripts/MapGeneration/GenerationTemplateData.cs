﻿using UnityEngine;
using System.Collections;

public class GenerationTemplateData : ScriptableObject {

    public int width = 60, height = 60;

    public int seed = -1;

    public Sprite[] tileset;
    public CustomRoomData[] customRooms;
    public ItemData[] enemies;

    public ItemData doorData;
    public ItemData doorKeyData;

    public int cutoutDepthThreshold = 2;
    public float splitProbability = 0.5f;
    public float customRoomProbability = 0.8f;
    public float randomRoomProbability = 0.5f;
    public float spawnProbability = 0.05f;

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Generator template")]
    public static void Create()
    {
        var asset = CreateInstance<GenerationTemplateData>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Generator template", "GeneratorTemplates");
    }
#endif //UNITY_EDITOR
}
