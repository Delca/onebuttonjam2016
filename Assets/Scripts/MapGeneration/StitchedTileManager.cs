﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StitchedTileManager {
    private static StitchedTileManager _instance = null;
    public static StitchedTileManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new StitchedTileManager();
            }

            return _instance;
        }
    }

    Sprite[] tileSet;
    IDictionary<string, Sprite> stitchedTiles;

    private StitchedTileManager()
    {
        stitchedTiles = new Dictionary<string, Sprite>();
    }

    public void SetTileSet(Sprite[] tileSet)
    {
        if (this.tileSet != tileSet)
        {
            this.tileSet = tileSet;
            stitchedTiles.Clear();
        }        
    }

    string getKey(int topLeft, int topRight, int bottomLeft, int bottomRight)
    {
        return topLeft + "-" + topRight + "-" + bottomLeft + "-" + bottomRight;
    }

    public Sprite GetTile(int topLeft, int topRight, int bottomLeft, int bottomRight)
    {
        string key = getKey(topLeft, topRight, bottomLeft, bottomRight);

        if (stitchedTiles.Keys.Contains(key))
        {
            return stitchedTiles[key];
        }

        int subWidth = (int)tileSet[0].rect.width, subHeight = (int)tileSet[0].rect.height;
        int width = 2 * subWidth, height = 2 * subHeight;

        Color[,][] subTiles = new Color[2, 2][];
        subTiles[1, 0] = tileSet[topLeft].texture.GetPixels((int)tileSet[topLeft].textureRect.x,
                                                               (int)tileSet[topLeft].textureRect.y,
                                                               (int)tileSet[topLeft].textureRect.width,
                                                               (int)tileSet[topLeft].textureRect.height);
        subTiles[0, 0] = tileSet[bottomLeft].texture.GetPixels((int)tileSet[bottomLeft].textureRect.x,
                                                               (int)tileSet[bottomLeft].textureRect.y,
                                                               (int)tileSet[bottomLeft].textureRect.width,
                                                               (int)tileSet[bottomLeft].textureRect.height);
        subTiles[1, 1] = tileSet[topRight].texture.GetPixels((int)tileSet[topRight].textureRect.x,
                                                               (int)tileSet[topRight].textureRect.y,
                                                               (int)tileSet[topRight].textureRect.width,
                                                               (int)tileSet[topRight].textureRect.height);
        subTiles[0, 1] = tileSet[bottomRight].texture.GetPixels((int)tileSet[bottomRight].textureRect.x,
                                                               (int)tileSet[bottomRight].textureRect.y,
                                                               (int)tileSet[bottomRight].textureRect.width,
                                                               (int)tileSet[bottomRight].textureRect.height);

        Color[] pixels = new Color[4 * subWidth * subHeight];

        for (int j = 0; j < height; ++j)
            for (int i = 0; i < width; ++i)
            {
                pixels[i + j * width] = subTiles[j / subWidth, i / subHeight][(i % subWidth) + (j % subHeight) * subWidth];
            }

        Texture2D texture = new Texture2D(width, height);
        texture.filterMode = FilterMode.Point;
        texture.SetPixels(pixels);
        texture.Apply();
        Sprite tile = Sprite.Create(texture, new Rect(0, 0, width, height), new Vector2(0.5f, 0.5f), tileSet[topLeft].pixelsPerUnit);

        stitchedTiles[key] = tile;

        return tile;
    }
}
