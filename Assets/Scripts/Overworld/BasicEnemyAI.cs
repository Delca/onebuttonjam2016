﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using System;
using System.Collections.Generic;

public class BasicEnemyAI : MovingObject
{
    public int trackingDistance = 10;
    public ItemData[] loot;

    public static AStar aStar = null;

    IList<AStar.Position> path;
    int turnsOutsideTrackingDistance = 0;
    public bool canStartBattle = false;

    public MusicManager.MusicTrack battleTheme = MusicManager.MusicTrack.BATTLE;

    void Awake()
    {
        if (aStar == null)
        {
            aStar = OverworldManager.instance.getAStar();
        }
    }

    public override void executeBehaviour()
    {
        if (MovingObject.player == null)
        {
            Debug.Log("Player entity has not been set yet");
            return;
        }

        if (ManhattanDistanceTo(MovingObject.player) < trackingDistance)
        {
            path = aStar.getPath(new AStar.Position(xMap, yMap), new AStar.Position(MovingObject.player.xMap, MovingObject.player.yMap));
            turnsOutsideTrackingDistance = 0;
        }
        else
        {
            ++turnsOutsideTrackingDistance;
        }

        if (turnsOutsideTrackingDistance > 10)
        {
            path = null;
        }

        if (path != null && path.Count > 0)
        {
            AStar.Position next = path[0];
            path.Remove(next);

            AttemptMove<Component>(next.x - xMap, next.y - yMap, false);
        }

        if (xMap == MovingObject.player.xMap && yMap == MovingObject.player.yMap)
        {
            canStartBattle = true;
        }
    }

    public void Update()
    {
        if (canStartBattle && OverworldManager.instance.AllMovementFinished() && !player.isMoving)
        {
            player.StartCoroutine(BattleSystem.OpenBattleScene(player.entities, entities, battleTheme));
            OverworldManager.instance.RemoveEntity(this);
            Destroy(gameObject);
            OverworldManager.instance.enabledCallback = () => {
                string[] dialogs = entities[Random.Range(0, entities.Count)].entityData.dialogs;

                if (dialogs.Length > 0)
                {
                    PlayerMessage.Display(dialogs[Random.Range(0, dialogs.Length)]);
                }

                OverworldManager.instance.ScatterItemsAround(player.xMap, player.yMap, new List<ItemData>(loot), true);
                player.isMoving = false;
                player.xMap = this.xMap;
                player.yMap = this.yMap;
            };
        }        
    }

    protected override void OnCantMove<T>(T component)
    {
        return;
    }

    protected override void OnMoveEnd()
    {
        return;
    }
}

