﻿using UnityEngine;
using System.Collections;
using System;

public class Item : MovingObject
{
    public ItemData itemData;

    SpriteRenderer spriteRenderer;
    protected bool canBeCollected = false;
    protected bool hasBeenCollected = false;

    protected override void Start()
    {
        base.Start();
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();

        if (itemData is UnlockableItem && ((UnlockableItem) itemData).keyToUnlock != null)
        {
            MakeBlocking();
        }

        UpdateSprite();
    }

    public virtual void Update()
    {
        if (canBeCollected && !player.isMoving)
        {
            CollectItem();
        }

        if (hasBeenCollected)
        {
            OverworldManager.instance.RemoveEntity(this);
            Destroy(gameObject);
        }
    }

    public void UpdateSprite()
    {
        spriteRenderer.sprite = itemData.sprite;
    }

    public override void executeBehaviour()
    {
        if (MovingObject.player == null)
        {
            Debug.Log("Player entity has not been set yet");
            return;
        }

        if (xMap == player.xMap && yMap == player.yMap)
        {
            canBeCollected = true;
        }
        else
        {
            canBeCollected = false;
        }
    }

    protected override void OnCantMove<T>(T component)
    {
        throw new NotImplementedException();
    }

    public virtual void CollectItem()
    {
        if (hasBeenCollected)
        {
            return;
        }

        hasBeenCollected = itemData.OnItemPickup(this);
    }

    public void MakeBlocking()
    {
        boxCollider.isTrigger = false;
        gameObject.layer = LayerMask.NameToLayer("BlockingLayer");
    }

    public void MakePassthrough()
    {
        boxCollider.isTrigger = true;
        gameObject.layer = LayerMask.NameToLayer("Default");
    }
}
