﻿using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour
{
    public GameObject overworldManager;
    public GameObject musicManager;
	// Use this for initialization
	void Awake ()
	{
	    if (OverworldManager.instance == null)
        {
            var overworldInstance = Instantiate(overworldManager);
            overworldInstance.transform.SetParent(transform, false);
        }
        if (GetComponentInChildren<MusicManager>() == null)
        {
            GameObject musicManagerInstance = Instantiate(musicManager);
            //musicManagerInstance.transform.SetParent(transform, false);
        }
	}
}
