﻿using UnityEngine;
using System.Collections;
using Utils;
using System;

public class MovementController : MonoBehaviour {

    public MovingObject target = null;
    public float tempo = 80;
    public float distanceFromCenter = 0.20f;

    public Constant.Direction[] sequence = new Constant.Direction[] {Constant.Direction.Up, Constant.Direction.Left, Constant.Direction.Right, Constant.Direction.Down};
    int currentStepInSequence = 0;
    float lastStepChange;

	// Use this for initialization
	void Start () {
        if (target != null)
        {
            transform.parent = target.transform;
        }
        UpdateSprite();
        lastStepChange = Time.realtimeSinceStartup;
    }

    // Update is called once per frame
    void Update () {
        float now = Time.realtimeSinceStartup;
        bool hasInput = Input.GetButton("Button");

        if (!hasInput && now - lastStepChange > 60 / tempo)
        {
            currentStepInSequence = ++currentStepInSequence % sequence.Length;
            UpdateSprite();
            lastStepChange = now;
        }

        if (hasInput)
        {
            AcceptInput();
        }
	}

    void AcceptInput()
    {
        Constant.GetDirectionFromEnum(sequence[currentStepInSequence], out target.xDir, out target.yDir);
        if (OverworldManager.instance.PlayTurn())
        {
            if (target != null)
            {
                lastStepChange = Time.realtimeSinceStartup + target.moveTime;
            }
        }        
    }

    void UpdateSprite ()
    {
        switch (sequence[currentStepInSequence])
        {
            default:
            case Constant.Direction.Up:
                transform.localPosition = new Vector3(0, distanceFromCenter);
                transform.localEulerAngles = new Vector3(0, 0, 0);
                break;
            case Constant.Direction.Down:
                transform.localPosition = new Vector3(0, -distanceFromCenter);
                transform.localEulerAngles = new Vector3(0, 0, 180);
                break;
            case Constant.Direction.Left:
                transform.localPosition = new Vector3(-distanceFromCenter, 0);
                transform.localEulerAngles = new Vector3(0, 0, 90);
                break;
            case Constant.Direction.Right:
                transform.localPosition = new Vector3(distanceFromCenter, 0);
                transform.localEulerAngles = new Vector3(0, 0, 270);
                break;
        }
    }

    void RotateSequence()
    {
        Constant.Direction targetValue = sequence[currentStepInSequence];
        
        if (sequence[0] != targetValue)
        {
            int shift = (int)targetValue - (int)sequence[0];
            int numberOfDirections = Enum.GetValues(typeof(Constant.Direction)).Length;

            for (int i = 0; i < sequence.Length; ++i)
            {
                sequence[i] = (Constant.Direction)(((int)sequence[i] - shift) % numberOfDirections);
            }
        }
    }
}
