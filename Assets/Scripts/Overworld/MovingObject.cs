﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour, IComparable<MovingObject>
{
    public static Player player = null;

    public float moveTime;
    public int speed = 0;
    public LayerMask[] blockingLayers;

    public List<EntityData> entityData;
    public List<Entity> entities = new List<Entity>();

    protected BoxCollider2D boxCollider;
    private Rigidbody2D rb2D;

    public bool isMoving = false;
    protected bool finishedMovement = true;

    public static GameObject mainCamera = null;

    //[HideInInspector]
    public int xMap, yMap;
    [HideInInspector]
    public int xDir, yDir;
    // Use this for initialization
    protected virtual void Start()
    {
        mainCamera = Camera.main.gameObject;
        foreach (var dataEntry in entityData)
            entities.Add(new Entity(dataEntry));
        
        boxCollider = GetComponent<BoxCollider2D>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    protected IEnumerator MoveTo(Vector3 targetPos)
    {
        isMoving = true;
        finishedMovement = false;

        var timer = moveTime;
        var originalPosition = transform.position;

        while ((transform.position - targetPos).sqrMagnitude > Vector3.kEpsilon)
        {
            var progress = Mathf.Max(0, Mathf.Min(1, 1 - timer / moveTime));
            transform.position = Vector3.Lerp(originalPosition, targetPos, progress);

            if (this == player)
            {
                mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, mainCamera.transform.position.z);
            }

            timer -= Time.smoothDeltaTime;
            yield return null;
        }
        transform.position = targetPos;

        isMoving = false;
        OnMoveEnd();
    }

    protected virtual void OnMoveEnd()
    {
    }

    public abstract void executeBehaviour();
    protected abstract void OnCantMove<T>(T component)
        where T : Component;

    protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
    {
        TileInfo t = OverworldManager.instance.levelGenerator.getTileAt(xMap + xDir, yMap + yDir, new TileInfo(TileInfo.Type.HardenedWall));

        if (t.type == TileInfo.Type.CorridorWall
            || t.type == TileInfo.Type.RoomWall
            || t.type == TileInfo.Type.HardenedWall
            // || (t.content is UnlockableItem && (t.content as UnlockableItem).keyToUnlock != null)
            )
        {
            hit = new RaycastHit2D();
            return false;
        }

        Vector2 start = transform.position;
        Vector2 target = start + new Vector2(xDir * Constant.tileWidth, yDir * Constant.tileHeight);

        boxCollider.enabled = false;
        for (int i = 0; i < blockingLayers.Length; ++i)
        {
            LayerMask blockingLayer = blockingLayers[i];
            hit = Physics2D.Linecast(start, target, blockingLayer);
            if (hit.transform != null)
            {
                boxCollider.enabled = true;
                return false;
            }
        }
        boxCollider.enabled = true;

        hit = new RaycastHit2D();
        xMap += xDir;
        yMap += yDir;
        StartCoroutine(MoveTo(target));

        return true;
    }

    protected virtual bool AttemptMove<T>(int xDir, int yDir, bool allowToWalkIntoEnemy = true)
        where T : Component
    {
        if (isMoving)
        {
            return false;
        }

        if (this == player)
        {
            boxCollider.enabled = false;
            RaycastHit2D floorTile;

            Vector2 start = transform.position;
            Vector2 target = start + new Vector2(xDir * Constant.tileWidth / 3, yDir * Constant.tileHeight / 3);
            
                LayerMask blockingLayer = LayerMask.GetMask("FloorLayer");
                floorTile = Physics2D.Linecast(start, target, blockingLayer);
                
                boxCollider.enabled = true;

            if (floorTile.transform != null)
            {
                string[] splitName = floorTile.transform.gameObject.name.Split(',');

                try
                {
                    xMap = Int32.Parse(splitName[0].Substring(splitName[0].IndexOf("[") + 1));
                    yMap = Int32.Parse(splitName[1].Substring(0, splitName[1].IndexOf("]")));
                    Debug.Log("FLOOR");
                }
                catch (Exception e)
                {
                    Debug.Log("HIT " + floorTile.transform.gameObject.name);
                }

            }
        }

        if (!allowToWalkIntoEnemy)
        {
            foreach (MovingObject mO in OverworldManager.instance.entities)
            {
                if (mO != player)
                {
                    if (xMap + xDir == mO.xMap && yMap + yDir == mO.yMap)
                    {
                        return false;
                    }
                }
            }
        }        

        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir, out hit);
        
        if (hit.transform != null)
        {
            T hitComponent = hit.transform.GetComponent<T>();

            if (!canMove && hitComponent != null)
            {
                OnCantMove<T>(hitComponent);
            }
        }

        return canMove;
    }

    public int ManhattanDistanceTo(MovingObject o)
    {
        return Math.Abs(xMap - o.xMap) + Math.Abs(yMap - o.yMap);
    }

    int IComparable<MovingObject>.CompareTo(MovingObject other)
    {
        return other.speed - speed;
    }
}
