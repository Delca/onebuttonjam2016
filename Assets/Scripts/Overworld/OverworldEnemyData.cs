﻿using UnityEngine;
using System.Collections;

public class OverworldEnemyData : ScriptableObject {

    public Sprite overworldSprite;
    public EntityData[] battleUnits;
    public ItemData[] loot;
    public MusicManager.MusicTrack battleTheme = MusicManager.MusicTrack.BATTLE;
    public int trackingDistance = 10;

#if UNITY_EDITOR
    [UnityEditor.MenuItem(AssetUtils.MENU_PATH + "Overworld Enemy")]
    public static void Create()
    {
        var asset = CreateInstance<OverworldEnemyData>();
        AssetUtils.CreateAssetAtCurrentPath(asset, "Overworld Enemy", "OverworldEnemies");
    }
#endif //UNITY_EDITOR
}
