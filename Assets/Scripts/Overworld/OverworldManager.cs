﻿using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections;
using System;
using Position = AStar.Position;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class OverworldManager : MonoBehaviour
{
    public static OverworldManager instance = null;

    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public GameObject itemPrefab;

    public GameObject player = null;

    public DungeonLevelGenerator levelGenerator;
    public List<MovingObject> entities;
    IList<MovingObject> entitiesToRemove;
    GameObject entitiesGameObject;
    float lastPlayedTurn;

    public int currentDepth = 0;
    public GenerationTemplateData[] levels;

    public GenerationTemplateData bossLevel;
    public bool isBossLevel = false;

    public Action enabledCallback = null;
    EnemyBalancer enemyBalancer = null;

    void Awake()
    {
        Application.targetFrameRate = 60;
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        entities = new List<MovingObject>();
        entitiesToRemove = new List<MovingObject>();
        entitiesGameObject = new GameObject();
        entitiesGameObject.name = "Entities";
        entitiesGameObject.transform.parent = transform;
        lastPlayedTurn = Time.realtimeSinceStartup - 2 * Constant.minimumDurationBetweenTurns/3;

        levelGenerator = GetComponent<DungeonLevelGenerator>();
        enemyBalancer = GetComponent<EnemyBalancer>();
    }

    void OnEnable()
    {
        if (enabledCallback != null)
        {
            enabledCallback();
        }
        lastPlayedTurn = Time.realtimeSinceStartup + 0.6f;
    }

    public AStar getAStar(Func<TileInfo, bool> isWall = null)
    {
        return levelGenerator.getAStar(isWall);
    }

    void Start()
    {
        currentDepth = 0;
        RegenerateLevel(false);
    }

    public void GoToNextLevel()
    {
        ++currentDepth;
        currentDepth = Math.Min(currentDepth + 1, levels.Length - 1);
        RegenerateLevel();
    }

    public void GoToBossLevel()
    {
        isBossLevel = true;
        RegenerateLevel();
    }

    private void InitLevel()
    {
        if (currentDepth >= levels.Length)
        {
            Debug.LogErrorFormat("The current depth({0}) exceeds the max authorized depth({1}).\n Returning to title.", currentDepth, levels.Length);
            SceneManager.LoadScene("Title");
            return;
        }

        levelGenerator.generateLevel((isBossLevel ? bossLevel : levels[currentDepth]));
        for (var i = 110; i < levelGenerator.itemsDatabase.Length; ++i)
        {
            if (levelGenerator.itemsDatabase[i] is CollectibleItem)
                levelGenerator.placeRandomItems(TileInfo.Type.RoomFloor, levelGenerator.itemsDatabase[i], 16);
        }
        levelGenerator.instantiateLevel();

        BasicEnemyAI.aStar = getAStar();
    }

    private void CleanUpLevel()
    {
        foreach (MovingObject mo in entities)
        {
            if (mo.name != player.name)
            {
                Destroy(mo.gameObject);
            }
        }
        entities.Clear();
        levelGenerator.Clear();
        //MovingObject.player = null;
    }

    public bool PlayTurn()
    {
        float now = Time.realtimeSinceStartup;

        if (now - lastPlayedTurn < Constant.minimumDurationBetweenTurns)
        {
            return false;
        }

        foreach (MovingObject mO in entitiesToRemove)
        {
            entities.Remove(mO);
        }
        entitiesToRemove.Clear();

        entities.Sort();
        foreach (MovingObject mO in entities) {
            mO.executeBehaviour();
            if (mO is BasicEnemyAI && (mO as BasicEnemyAI).canStartBattle)
            {
                break;
            }
        }

        lastPlayedTurn = Time.realtimeSinceStartup;

        return true;
    }

    void SpawnPlayer()
    {
        GameObject tileGameObject = GameObject.Find("Tile@[" + levelGenerator.spawnPoint.x + "," + levelGenerator.spawnPoint.y + "]");

        if (player == null)
        {
            player = Instantiate(playerPrefab, tileGameObject.transform.position, Quaternion.identity) as GameObject;
        }
        else
        {
            player.transform.position = tileGameObject.transform.position;
        }
        MovingObject playerMO = player.GetComponent<MovingObject>();

        playerMO.xMap = levelGenerator.spawnPoint.x;
        playerMO.yMap = levelGenerator.spawnPoint.y;

        AddEntity(player.GetComponent<MovingObject>());
    }

    void SpawnEnemy(Tile tile, OverworldEnemyData enemyData, ItemData[] loot = null)
    {
        GameObject enemy = Instantiate(enemyPrefab, tile.transform.position, Quaternion.identity) as GameObject;
        enemy.GetComponent<SpriteRenderer>().sprite = enemyData.overworldSprite;
        BasicEnemyAI enemyAI = enemy.GetComponent<BasicEnemyAI>();

        enemyAI.entityData = new List<EntityData>(enemyData.battleUnits);
        enemyAI.loot = loot;
        enemyAI.battleTheme = enemyData.battleTheme;
        enemyAI.trackingDistance = enemyData.trackingDistance;

        MovingObject enemyMO = enemy.GetComponent<MovingObject>();

        enemyMO.xMap = tile.x;
        enemyMO.yMap = tile.y;

        AddEntity(enemy.GetComponent<MovingObject>());
    }

    public void SpawnItem(int x, int y, ItemData itemData)
    {
        Tile tileGameObject = GameObject.Find("Tile@[" + x + "," + y + "]").GetComponent<Tile>();

        if (itemData is GuardedItem)
        {
            GuardedItem guardedItem = itemData as GuardedItem;
            SpawnEnemy(tileGameObject, guardedItem.guard, guardedItem.loot);
        }
        else
        {
            GameObject item = Instantiate(itemPrefab, tileGameObject.transform.position, Quaternion.identity) as GameObject;
            MovingObject itemMO = item.GetComponent<MovingObject>();
            itemMO.xMap = x;
            itemMO.yMap = y;
            item.name = "Item (" + itemData.name.ToString() + ")";
            item.GetComponent<Item>().itemData = itemData;
            AddEntity(item.GetComponent<MovingObject>());
        }
    }

    public void AddEntity(MovingObject entity)
    {
        entities.Add(entity.GetComponent<MovingObject>());
        entity.transform.SetParent(entitiesGameObject.transform, false); ;
    }

    public void RemoveEntity(MovingObject entity)
    {
        entitiesToRemove.Add(entity);
    }

    public void ScatterItemsAround(int x, int y, IList<ItemData> items, bool excludeCenter = false)
    {
        IList<Position> freeSpaces = new List<Position>();
        IList<string> alreadyCheckedSpaces = new List<string>();
        IList<Position> spacesToTry = new List<Position>();

        TileInfo defaultTile = new TileInfo(TileInfo.Type.CorridorWall);

        spacesToTry.Add(new Position(x, y));

        while (spacesToTry.Count > 0 && freeSpaces.Count < items.Count)
        {
            Position currentPos = spacesToTry[0];
            spacesToTry.Remove(currentPos);
            alreadyCheckedSpaces.Add(currentPos.x + "-" + currentPos.y);

            TileInfo currentTile = levelGenerator.getTileAt(currentPos.x, currentPos.y, defaultTile);

            if (currentTile.type == TileInfo.Type.CorridorFloor || currentTile.type == TileInfo.Type.RoomFloor)
            {
                bool occupied = false;

                foreach (GameObject itemGO in GameObject.FindGameObjectsWithTag("Item"))
                {
                    MovingObject itemMO = itemGO.GetComponent<MovingObject>();

                    if (itemMO.xMap == currentPos.x && itemMO.yMap == currentPos.y)
                    {
                        occupied = true;
                        break;
                    }
                }

                if (!occupied && (excludeCenter == false || currentPos.x != x || currentPos.y != y))
                {
                    freeSpaces.Add(new Position(currentPos.x, currentPos.y));
                }
            }

            List<Position> spacesToAdd = new List<Position>();

            spacesToAdd.Add(new Position(currentPos.x - 1, currentPos.y));
            spacesToAdd.Add(new Position(currentPos.x + 1, currentPos.y));
            spacesToAdd.Add(new Position(currentPos.x, currentPos.y - 1));
            spacesToAdd.Add(new Position(currentPos.x, currentPos.y + 1));

            while (spacesToAdd.Count > 0)
            {
                Position pos = spacesToAdd[Random.Range(0, spacesToAdd.Count - 1)];
                spacesToAdd.Remove(pos);

                spacesToTry.Add(pos);
            }

        }

        for (int i = 0; i < freeSpaces.Count; ++i)
        {
            Position itemPos = freeSpaces[i];
            SpawnItem(itemPos.x, itemPos.y, items[i]);
        }
    }

    public void RegenerateLevel(bool rebalanceEnemies = true)
    {
        CleanUpLevel();

        if (rebalanceEnemies)
        {
            enemyBalancer.RebalanceEnemies();
        }

        InitLevel();
        SpawnPlayer();
        if (MusicManager.instance != null)
        {
            MusicManager.instance.PlayTrack(MusicManager.MusicTrack.DUNGEON);
        }
    }

    public bool AllMovementFinished()
    {
        foreach (MovingObject mO in entities)
        {
            if (mO.isMoving)
            {
                return false;
            }
        }

        return true;
    }
}
