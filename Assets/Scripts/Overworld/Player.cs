﻿using UnityEngine;
using System.Collections;
using System;

public class Player : MovingObject
{
    void Awake()
    {
        
        if (MovingObject.player == null)
        {
            MovingObject.player = this;
        }

        isMoving = false;
        finishedMovement = true;
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        xDir = 0;
        yDir = 0;
        mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, mainCamera.transform.position.z);
        EnemyBalancer.players = entities.ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, mainCamera.transform.position.z);

        xDir = (int)Input.GetAxisRaw("Horizontal");
        yDir = (int)Input.GetAxisRaw("Vertical");

        if (!isMoving && finishedMovement && (xDir != 0 || yDir != 0))
        {
            OverworldManager.instance.PlayTurn();
        }

        if (!isMoving && !finishedMovement)
        {
            finishedMovement = true;
        }
    }

    protected override void OnCantMove<T>(T component)
    {
        Item item = component.GetComponent<Item>();
        if (item != null)
        {
            UnlockableItem itemData = (item as Item).itemData as UnlockableItem;

            if (itemData != null && itemData.keyToUnlock != null)
            {
                ItemData key = PlayerInventory.instance.GetItem<ItemData>(itemData.keyToUnlock);

                if (key != null)
                {
                    item.CollectItem();
                    SoundEffectManager.instance.PlaySound(SoundEffectManager.SoundEffect.COLLECTKEY);
                }
            }
        }
    }

    public override void executeBehaviour()
    {
        if (xDir != 0)
        {
            yDir = 0;
        }

        if (xDir != 0 || yDir != 0)
        {
          AttemptMove<Component>(xDir, yDir);
        }
    }
}
