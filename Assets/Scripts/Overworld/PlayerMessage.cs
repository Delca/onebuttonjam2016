﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerMessage : MonoBehaviour
{
    static PlayerMessage instance_;

    public int visibleCharCount;
    public float messageDisplayTime = 10f;
    public Text messageLabel;

    float lastMessageDisplayed;

    void Awake()
    {
        Debug.Assert(instance_ == null);
        instance_ = this;
        messageLabel = GetComponentInChildren<Text>();
        lastMessageDisplayed = -messageDisplayTime;
        SetMessage("");        
    }

    public static void Display(string message)
    {
        if (instance_ != null)
            instance_.SetMessage(message);
        else
            Debug.Log("No instance to display the message.");
    }

    void SetMessage(string message)
    {
        if (message.Length > visibleCharCount)
            Debug.Log("The message is too long and will be truncated.");
        messageLabel.text = message;
        lastMessageDisplayed = Time.time;
    }

    void Update()
    {
        if (Time.time - lastMessageDisplayed > messageDisplayTime)
        {
            messageLabel.text = "";
        }
    }
}
