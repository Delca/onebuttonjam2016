﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

    public TileInfo tileInfo;
    public int x, y;
}
