﻿using System.Collections.Generic;
using UnityEngine;

public class PixelPerfectScaling : MonoBehaviour
{
    public float scale { get; private set; }
    public Vector2 originalSize { get; private set; }

    void Awake()
    {
        var rectTransform = transform as RectTransform;
        Debug.Assert(rectTransform != null);

        originalSize = rectTransform.rect.size;

        SetMaxScale();
    }

#if UNITY_EDITOR
    void Update()
    {
        SetMaxScale();
    }
#endif // UNITY_EDITOR

    public void SetScale(float scale, bool fullScreen = false)
    {
        this.scale = scale;
        transform.localScale = Vector3.one * Mathf.FloorToInt(scale);
        Screen.SetResolution(Mathf.FloorToInt(originalSize.x * scale), Mathf.FloorToInt(originalSize.y * scale), fullScreen);
    }

    public float ComputeMaxAvailableScale()
    {
        float maxScale = ComputeMaxAvailableScale(Screen.width, Screen.height);
        Debug.Assert(maxScale >= 1.0f);
        return maxScale;
    }

    public float ComputeMaxAvailableScale(IEnumerable<Resolution> resolutions)
    {
        float maxScale = 0.0f;
        foreach (var resolution in resolutions)
            maxScale = Mathf.Max(maxScale, ComputeMaxAvailableScale(resolution.width, resolution.height));
        return maxScale;
    }

    public float ComputeMaxAvailableScale(int screenWidth, int screenHeight)
    {
        float horizontalScale = screenWidth / originalSize.x;
        float verticalScale = screenHeight / originalSize.y;

        return Mathf.Min(horizontalScale, verticalScale);
    }

    public void SetMaxScale(bool fullScreen = false)
    {
        SetScale(ComputeMaxAvailableScale(), fullScreen);
    }
}
