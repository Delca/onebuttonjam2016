﻿using UnityEngine;

public static class TransformExtention
{
    public static void DestroyChildren(this Transform transform, int startIndex = 0)
    {
        int childCount = transform.childCount;
        for (int i = startIndex; i < childCount; i++)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }
}