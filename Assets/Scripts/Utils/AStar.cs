﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AStar
{
    public class Position
    {
        public int x, y;
        public int priority;

        public Position(int x, int y, int priority = -1)
        {
            this.x = x;
            this.y = y;
            this.priority = priority;
        }

        public override string ToString()
        {
            return x + "," + y;
        }

        public bool IsSame(Position o)
        {
            return x == o.x && y == o.y;
        }
    };

    TileInfo[,] map;
    int width, height;
    Func<TileInfo, bool> isWall;

    public AStar(TileInfo[,] map, int width, int height, Func<TileInfo, bool> isWall = null)
    {
        this.map = map;
        this.width = width;
        this.height = height;
        this.isWall = isWall;

        if (this.isWall == null)
        {
            this.isWall = (TileInfo t) => {
                return t.type == TileInfo.Type.CorridorWall || t.type == TileInfo.Type.RoomWall || t.type == TileInfo.Type.HardenedWall || (t.content is UnlockableItem && (t.content as UnlockableItem).keyToUnlock != null);
            };
        }
    }

    public IList<Position> getPath(Position from, Position to)
    {
        IList<Position> frontier = new List<Position>();
        IDictionary<string, string> cameFrom = new Dictionary<string, string>();
        IDictionary<string, int> scoreSoFar = new Dictionary<string, int>();

        if (isWall(map[to.x, to.y]))
        {
            return buildPath(cameFrom, to);
        }

        scoreSoFar[from + ""] = 0;
        cameFrom[from + ""] = "NONE";

        Position current;

        frontier.Add(from);
        while (frontier.Count > 0)
        {
            current = frontier[0];
            frontier.Remove(current);

            if (current.IsSame(to))
            {
                break;
            }

            foreach (Position next in getNeighbours(current))
            {
                int newScore = scoreSoFar[current + ""] + getCost(current, next);
                if (!scoreSoFar.Keys.Contains(next + "") || newScore < scoreSoFar[next +  ""])
                {
                    scoreSoFar[next + ""] = newScore;
                    next.priority = getHeuristicOn(next, to);
                    if (!isWall(map[next.x, next.y]))
                        frontier.Add(next);
                    cameFrom[next + ""] = current + "";
                }
            }

        }

        return buildPath(cameFrom, to);
    }

    public int getCost(Position current, Position next)
    {
        if (map[next.x, next.y].type == TileInfo.Type.RoomWall || map[next.x, next.y].type == TileInfo.Type.CorridorWall || map[next.x, next.y].type == TileInfo.Type.HardenedWall)
        {
            return width * height;
        }

        return 1;
    }

    public int getHeuristicOn(Position next, Position to)
    {
        return Math.Abs(next.x - to.x) + Math.Abs(next.y - to.y);
    }

    private IList<Position> buildPath(IDictionary<string, string> cameFrom, Position to)
    {
        List<Position> ans = new List<Position>();

        if (!cameFrom.Keys.Contains(to + ""))
        {
            // The target is unreachable
             return ans;
        }

        Position current = to;

        while (cameFrom[current + ""] != "NONE")
        {
            ans.Add(current);
            string nextStep = cameFrom[current + ""];
            current = new Position(0, 0);
            current.x = Int32.Parse(nextStep.Split(',')[0]);
            current.y = Int32.Parse(nextStep.Split(',')[1]);
        }

        ans.Reverse();

        return ans;
    }

    IList<Position> getNeighbours(Position cell)
    {
        IList<Position> ans = new List<Position>();

        if (cell.x - 1 >= 0)
        {
            ans.Add(new Position(cell.x - 1, cell.y));
        }
        if (cell.x + 1 < width)
        {
            ans.Add(new Position(cell.x + 1, cell.y));
        }
        if (cell.y - 1 >= 0)
        {
            ans.Add(new Position(cell.x, cell.y - 1));
        }
        if (cell.y + 1 < height)
        {
            ans.Add(new Position(cell.x, cell.y + 1));
        }

        return ans;
    }
}
