﻿#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEngine;
#endif //UNITY_EDITOR

public static class AssetUtils
{
    public const string RESOURCES_ROOT_PATH = "Data";
#if UNITY_EDITOR
    public const string ASSET_ROOT_PATH = "Assets/Resources/" + RESOURCES_ROOT_PATH;
    public const string MENU_PATH = "Assets/Create/";

    public static void CreateAssetAtCurrentPath(ScriptableObject asset, string assetName, string defaultPath)
    {
        string path = ASSET_ROOT_PATH;
        if (defaultPath != "")
            path += "/" + defaultPath;

        foreach (Object obj in Selection.GetFiltered(typeof(Object), SelectionMode.Assets))
        {
            path = AssetDatabase.GetAssetPath(obj);
            if (File.Exists(path))
                path = Path.GetDirectoryName(path);
            break;
        }

        path += "/" + assetName + ".asset";
        AssetDatabase.CreateAsset(asset, path);

        Selection.activeObject = asset;
        EditorUtility.FocusProjectWindow();
    }
#endif //UNITY_EDITOR
}