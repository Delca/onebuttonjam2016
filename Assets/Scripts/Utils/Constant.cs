﻿using UnityEngine;
using System.Collections;

public class Constant
{
    public static float tileWidth = 1f;
    public static float tileHeight= 1f;
    public static float minimumDurationBetweenTurns = 0.2f;

    public enum Direction { Up, Left, Down, Right };

    public static void GetDirectionFromEnum(Direction dir, out int xDir, out int yDir)
    {
        xDir = (dir == Direction.Right ? 1 : 0);
        xDir = (dir == Direction.Left ? -1 : xDir);
        yDir = (dir == Direction.Up ? 1 : 0);
        yDir = (dir == Direction.Down ? -1 : yDir);
    }
};
