﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileInfo
{
    public enum Type { CorridorFloor, RoomFloor, CorridorWall, RoomWall, HardenedWall };
    
    public Type type;
    public ItemData content;
    public int variation;

    public TileInfo(Type t, int v = 0, ItemData c = null)
    {
        this.type = t;
        this.variation = v;
        this.content = c;
    }

    public override string ToString()
    {
        return "[" + type.ToString()[0] + content + "]";
    }
};
