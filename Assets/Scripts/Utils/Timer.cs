﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Utils
{
    public class Timer
    {
        IDictionary<string, float> startTimes;
        IDictionary<string, float> endTimes;

        public static Timer global = new Timer();

        public Timer()
        {
            startTimes = new Dictionary<string, float>();
            endTimes = new Dictionary<string, float>();
        }

        public void startTimer(string marker)
        {
            if (startTimes.Keys.Contains(marker))
            {
                Debug.Log("Marker " + marker + " has already been timed");
                return;
            }

            startTimes[marker] = Time.realtimeSinceStartup;
        }

        public float stopTimer(string marker)
        {
            float endTime = Time.realtimeSinceStartup;

            if (!startTimes.Keys.Contains(marker))
            {
                Debug.LogError("Marker " + marker + " has yet to be started");
                return -1;
            }

            if (endTimes.Keys.Contains(marker))
            {
                Debug.LogError("Marker " + marker + " has already been stopped");
                return -1;
            }

            endTimes[marker] = endTime;

            return durationOf(marker);
        }

        public float durationOf(string marker, bool ignoreRunning = false)
        {
            if (!startTimes.Keys.Contains(marker))
            {
                Debug.LogError("Marker " + marker + " has yet to be started");
                return -1;
            }

            if (!endTimes.Keys.Contains(marker))
            {
                if (!ignoreRunning)
                {
                    Debug.LogError("Marker " + marker + " has already been stopped");
                }                    
                return -1;
            }

            return endTimes[marker] - startTimes[marker];
        }

        public void Log()
        {
            string log = "";

            foreach (string key in startTimes.Keys)
            {
                log += key + ": ";

                if (endTimes.Keys.Contains(key))
                    log += durationOf(key, true);
                else
                    log += "running";

                log += '\n';
            }

            Debug.Log(log);
        }
    }
}